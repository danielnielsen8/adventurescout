#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='adventurescout_league',
    version='1.0',
    description="",
    author="",
    author_email='',
    url='',
    packages=find_packages(),
    package_data={'adventurescout_league': ['static/*.*', 'templates/*.*']},
    scripts=['manage.py'],
)
