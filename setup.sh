# Python version
PYTHONVER=2.7
PYTHON=python${PYTHONVER}

# Make sure dir exists
mkdir -p $HOME/lib/${PYTHON}

# Install PIP
easy_install-${PYTHONVER} pip
PIP=pip-${PYTHONVER}

# Install requirements
# in local python dir
$PIP install --install-option="--install-scripts=$PWD/bin" --install-option="--install-lib=$PWD/lib/$PYTHON" -r $PWD/project/requirements.txt