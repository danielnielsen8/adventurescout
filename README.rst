.. 

adventurescout_league
======================

Quickstart
----------

To bootstrap the project::

    virtualenv env/adventurescout_league
    source env/adventurescout_league/bin/activate
    pip install -r requirements.pip
    pip install -e .
    cp adventurescout_league/settings/local.py.example adventurescout_league/settings/local.py
    manage.py syncdb --migrate

adventurespejd production (solaris server)
------------------------------------------

Adventurespejd is located at /srv/webapps/adventurespejd/, in this folder the virtualenv is installed and the git repository placed.
Meaning that git commands can be performed in /srv/webapps/adventurespejd/adventurescout_league/, so most likely this will mean pulling new updates creating on a development enviroment.

To start, stop and restart the server on the production server supervisorctl is used::

    sudo service supervisorctl adventurespejd COMMAND

COMMAND should be either start, stop or restart.

Robin: on this box, the seemingly working commands are:
sudo service supervisor stop
sudo service supervisor start

The restart commands does nothing...
Or just say: kill -HUP <PID of gunicorn master process>

Database is located at: http://phppgadmin.dev.solaris.dk/

Documentation
-------------

Developer documentation is available in Sphinx format in the docs directory.

Initial installation instructions (including how to build the documentation as
HTML) can be found in docs/install.rst.
