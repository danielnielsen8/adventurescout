# -*- coding: utf-8

from django.http import Http404
from django.contrib.auth.decorators import login_required

from rest_framework.decorators import APIView, api_view
from rest_framework.response import Response
from rest_framework import authentication, permissions
from rest_framework.authtoken.models import Token

from adventurescout_league.apps.races.models import SeasonalRace
from adventurescout_league.apps.races.serializers import *

from adventurescout_league.apps.patrols.models import SeasonalPatrol
from adventurescout_league.apps.patrols.serializers import PublicSeasonalPatrolSerializer, SeasonalPatrolSerializer
from adventurescout_league.apps.registrations.models import SeasonalAdmission, Registration
from adventurescout_league.apps.registrations.serializers import SeasonalAdmissionSerializer, RegistrationSerializer
from adventurescout_league.apps.tokens.views import handle_token

# PUBLIC ######################################################


class RedeemToken(APIView):
    """
    Redeem data from token.

    * Public

    Example

        curl '[domain]/api/token/[token]'

    """
    def get(self, request, *args, **kwargs):
        data = handle_token(request, kwargs['token'])
        return Response(data)


class PatrolSeasonalPatrols(APIView):
    """
    Get seasonal patrols for patrol
    """
    def get(self, request, *args, **kwargs):
        sr = SeasonalPatrol.objects.filter(patrol__pk=kwargs['pk']).order_by(
            '-season__number')

        result_list = []
        current_season = Season.get_current_season()

        for patrol in sr:
            # Only show month in birth dates for current season
            include_month = patrol.season.number == current_season.number
            serializer = PublicSeasonalPatrolSerializer(patrol, include_month=include_month)
            result_list.append(serializer.data)

        return Response(result_list)


class RaceSeasonalAdmissions(APIView):
    """
    Get seasonal admissions for race
    """
    def get(self, request, *args, **kwargs):
        adms = SeasonalAdmission.objects.filter(
            seasonal_race__race__pk=kwargs['pk']).order_by(
            '-seasonal_race__season__number')
        s = SeasonalAdmissionSerializer(adms)
        return Response(s.data)


class RaceSeasonalRaces(APIView):
    """
    Get seasonal races for race
    """
    def get(self, request, *args, **kwargs):
        sr = SeasonalRace.objects.filter(race__pk=kwargs['pk']).order_by('-season__number')
        s = SeasonalRaceSerializer(sr, many=True)
        return Response(s.data)


# PRIVATE #####################################################

@login_required
@api_view(['GET'])
def user_token(request):
    if request.method == 'GET':
        t = Token.objects.get_or_create(user=request.user)[0]
        return Response({'token': t.key})


class SeasonalPatrolList(APIView):
    """
    Get a list of Seasonal Patrols

    * Requires token or session authentication
    * Only admin users are able to access this view.

    *Session authentication is only for viewing in the browser.*

    """
    authentication_classes = (authentication.SessionAuthentication, authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, *args, **kwargs):
        sp = SeasonalPatrol.objects.all()
        s = SeasonalPatrolSerializer(sp, many=True)
        return Response(s.data)


class SeasonalPatrolDetail(APIView):
    """
    Get the details of a Seasonal Patrol

    * Requires token or session authentication
    * Only admin users are able to access this view.

    *Session authentication is only for viewing in the browser.*

    Example

        curl 'http://liga.adventurespejd.dk/api/seasonal-patrol/02UP001/' -H 'Authorization: Token 0be3c80ab35cfa382a81ad24711d55966f523a2e' | python -m json.tool

    """
    authentication_classes = (authentication.SessionAuthentication, authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, *args, **kwargs):
        try:
            sp = SeasonalPatrol.get_object(league_id=kwargs['league_id'])
        except SeasonalPatrol.DoesNotExist:
            raise Http404

        s = SeasonalPatrolSerializer(sp)
        return Response(s.data)


class SeasonalRaceRegistrations(APIView):
    """
    Get a list of registration for a seasonal race
    """
    authentication_classes = (authentication.SessionAuthentication, authentication.TokenAuthentication,)
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, *args, **kwargs):
        try:
            seasonal_race = SeasonalRace.get_object(league_id=kwargs['league_id'])
        except SeasonalRace.DoesNotExist:
            raise Http404

        registrations = Registration.objects.filter(seasonal_admission__seasonal_race=seasonal_race)
        s = RegistrationSerializer(registrations)

        return Response(s.data)
