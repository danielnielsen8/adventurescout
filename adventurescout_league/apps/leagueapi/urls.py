# -*- coding: utf-8
from django.conf.urls import url
from adventurescout_league.apps.leagueapi.views import *

urlpatterns = [
    # Dev testing
    # url(r'^test/$', api_test),

    # Tokens
    url(r'^user-token/$', user_token),
    url(r'^token/(?P<token>.*)/$', RedeemToken.as_view()),

    # Seasonal patrol
    url(
        r'^seasonal-patrol/$', SeasonalPatrolList.as_view(), name='api_seasonal_patrol_list'
    ),
    url(
        r'^seasonal-patrol/(?P<league_id>\d{2}\w{2}\d{3})/$',
        SeasonalPatrolDetail.as_view(), name='api_seasonal_patrol_detail'
    ),
    # url(r'^seasonal-patrol/create/$', SeasonalPatrolCreate.as_view())

    # Data for widgets
    url(
        r'^patrol/(?P<pk>\d+)/seasonal-patrols/$',
        PatrolSeasonalPatrols.as_view(),
        name='api_patrol_seasonal_patrols'
    ),

    url(
        r'^race/(?P<pk>\d+)/seasonal-admissions/$', RaceSeasonalAdmissions.as_view(),
        name='api_race_seasonal_admissions'),
    url(
        r'^race/(?P<pk>\d+)/seasonal-races/$', RaceSeasonalRaces.as_view(),
        name='api_race_seasonal_races'
    ),

    # Registrations
    url(
        r'^seasonal-race/(?P<league_id>R\d{2}\w{2}\d{3})/registrations/$',
        SeasonalRaceRegistrations.as_view(),
        name='api_seasonal_race_registrations'
    ),
]
