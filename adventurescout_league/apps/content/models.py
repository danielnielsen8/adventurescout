# -*- coding: utf-8

from __future__ import unicode_literals

from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.utils.translation import ugettext_lazy as _
from django.utils.encoding import python_2_unicode_compatible

MARKDOWN_HELP_TEXT = (
    "Du kan bruge markdown syntax i dette felt."
    "Læs mere på 'http://daringfireball.net/projects/markdown/syntax'"
)


@python_2_unicode_compatible
class ContentText(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    content = models.TextField(_('Content'), blank=True, help_text=MARKDOWN_HELP_TEXT)

    def __str__(self):
        return self.name  


@python_2_unicode_compatible
class ContentPage(models.Model):
    url = models.CharField(_('URL'), max_length=100, db_index=True)
    title = models.CharField(_('Title'), max_length=200)
    content = models.TextField(_('Content'), blank=True, help_text=MARKDOWN_HELP_TEXT)
    aside_content = models.TextField(
        _('Aside content'), blank=True, help_text=MARKDOWN_HELP_TEXT)
    template_name = models.CharField(
        _('Template name'), max_length=70, blank=True,
        help_text=_("If this isn't provided, the system will use 'content/contentpage_default.html'."))

    cover_image = GenericRelation('CoverImage')

    class Meta:
        verbose_name = _('Content page')
        verbose_name_plural = _('Content pages')
        ordering = ('url',)

    def __str__(self):
        return "%s -- %s" % (self.url, self.title)

    def get_absolute_url(self):
        return self.url

    def format_content(self):
        return


class CoverImage(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    image = models.ImageField(upload_to='covers')

    class Meta:
        verbose_name = _('Cover image')
        verbose_name_plural = _('Cover images')
        unique_together = ('content_type', 'object_id',)
