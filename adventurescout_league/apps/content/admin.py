from django.contrib import admin
from django.contrib.contenttypes.admin import GenericTabularInline

from adventurescout_league.apps.content.models import ContentText, ContentPage, CoverImage
from adventurescout_league.apps.content.forms import ContentpageForm


class CoverImageInline(GenericTabularInline):
    model = CoverImage
    max_num = 1


class ContentPageAdmin(admin.ModelAdmin):
    form = ContentpageForm
    search_fields = ('url', 'title')
    inlines = [
        CoverImageInline,
    ]

admin.site.register(ContentText)
admin.site.register(ContentPage, ContentPageAdmin)