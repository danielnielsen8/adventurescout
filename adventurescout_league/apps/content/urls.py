from django.conf.urls import url

from adventurescout_league.apps.content.views import ContentPageDetail

urlpatterns = [
    url(r'^(?P<url>.*)$', ContentPageDetail.as_view(), name='content_contentpage'),
]
