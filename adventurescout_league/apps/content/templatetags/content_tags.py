from django.conf import settings
from django import template

from easy_thumbnails.files import get_thumbnailer

from adventurescout_league.apps.content.models import ContentText

base_options = {
    'quality': 95,
    'crop': 'smart',
    'upscale': 'true',
}
cover_options = {
    'size': (1176, 350,),
}
cover_options.update(base_options)

list_options = {
    'size': (358, 235,),
}
list_options.update(base_options)

register = template.Library()


@register.simple_tag
def cover_image(obj, use_default=True):
    try:
        image = obj.cover_image.all()[0]
        thumbnail = get_thumbnail(image.image, cover_options)
    except Exception as error:
        if type(error) not in (AttributeError, IndexError):
            raise
        if use_default:
            thumbnail = get_thumbnail(get_default_image('wide'), 
                                      cover_options, 'covers/default-image-wide.png')
        else:
            return ''

    return '<img src="{:s}" width="{:d}" height="{:d}" />'.format(thumbnail.url, 
                                                                  thumbnail.width, 
                                                                  thumbnail.height)


@register.simple_tag
def list_image(obj, use_default=True):
    try:
        image = obj.list_image.all()[0]
        thumbnail = get_thumbnail(image.image, list_options)
    except Exception as error:
        if type(error) not in (AttributeError, IndexError):
            raise
        if use_default:
            thumbnail = get_thumbnail(get_default_image('standard'), 
                                      list_options, 'covers/default-image.png')
        else:
            return ''

    return '<img src="{:s}" width="{:d}" height="{:d}" />'.format(thumbnail.url, 
                                                                  thumbnail.width, 
                                                                  thumbnail.height)

def get_thumbnail(image, options, relative_name=None):
    thumbnailer = get_thumbnailer(image, relative_name)
    return thumbnailer.get_thumbnail(options)

def get_default_image(variant):
    return open(settings.DEFAULT_IMAGES[variant], 'r')


@register.assignment_tag
def text_content(name):
    try:
        text = ContentText.objects.get(name=name)
        return text.content
    except ContentText.DoesNotExist:
        return ''
