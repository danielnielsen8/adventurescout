from django.conf import settings
from django.http import Http404, HttpResponsePermanentRedirect
from django.shortcuts import get_object_or_404, render
from django.views.generic.base import TemplateView

from adventurescout_league.apps.content.models import ContentPage


class ContentPageDetail(TemplateView):
    template_name = 'content/contentpage_detail.html'

    def get(self, request, url, **kwargs):
        # Try to resolve the provided 'url'
        # Knicked from django.contrib.flatpages 
        # - without 'sites' support
        if not url.startswith('/'):
            url = '/' + url
        try:
            page = get_object_or_404(ContentPage, url__exact=url)
            if page.template_name:
                self.template_name = page.template_name
        except Http404:
            if not url.endswith('/') and settings.APPEND_SLASH:
                return HttpResponsePermanentRedirect('{:s}/'.format(request.path))
            else:
                raise
        return render(request, self.template_name, context={'page': page})
