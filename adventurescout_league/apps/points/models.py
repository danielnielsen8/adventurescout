# -*- coding: utf-8

from __future__ import unicode_literals

from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from adventurescout_league.apps.league.models import Season
from adventurescout_league.apps.patrols.models import SeasonalPatrol
from adventurescout_league.apps.races.models import SeasonalRace

COMPLETION_SCORE = 10

POINT_KEY = {
        1: 100,
        2: 93,
        3: 86,
        4: 79,
        5: 72,
        6: 69,
        7: 66,
        8: 63,
        9: 60,
        10: 57,
        11: 54,
        12: 51,
        13: 48,
        14: 46,
        15: 44,
        16: 42,
        17: 40,
        18: 38,
        19: 36,
        20: 34,
        21: 32,
        22: 30,
        23: 28,
        24: 26,
        25: 24,
        26: 22,
        27: 20,
        28: 18,
        29: 16,
        30: 14,
        31: 12,
        32: 10,
        33: 8,
        34: 7,
        35: 6,
        36: 5,
        37: 4,
        38: 3,
        39: 2,
        40: 1,
        41: 0
}


class PointSetManager(models.Manager):
    def current(self):
        season = Season.get_current_season()
        try:
            return self.get(season=season)
        except self.model.DoesNotExist:
            return None

    def archived(self):
        season = Season.get_current_season()
        return self.exclude(season=season).order_by('season__number')


# A point set
@python_2_unicode_compatible
class Point(models.Model):

    class Meta:
        unique_together = (
            ('seasonal_patrol', 'seasonal_race'),
            ('seasonal_race', 'placement')
        )

    completed = models.BooleanField()
    disqualified = models.BooleanField()
    placement = models.PositiveIntegerField()

    seasonal_patrol = models.ForeignKey(SeasonalPatrol, on_delete=models.CASCADE)
    seasonal_race = models.ForeignKey(SeasonalRace, on_delete=models.CASCADE)

    def __str__(self):
        return self.seasonal_patrol

    # Get total score for this point set
    def get_total_score(self):
        if self.completed:
            c = COMPLETION_SCORE
        else:
            c = 0

        try:
            return c + POINT_KEY[self.placement]
        except KeyError:
            return 0


@python_2_unicode_compatible
class PointSet(models.Model):
    serialized_data = models.TextField(blank=True)
    html_data = models.TextField(blank=True)
    published = models.BooleanField(default=False)

    season = models.ForeignKey(Season, on_delete=models.CASCADE)

    objects = PointSetManager()

    def __str__(self):
        return 'Pointset : {}'.format(self.season)

    @property
    def mode(self):
        # empty unicode string is implicit false 
        if self.html_data:
            return 'html'
        elif self.serialized_data:
            return 'json'
        else:
            return ''

