# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.points.models import Point, PointSet


class PointSetAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'published',)


class PointAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'seasonal_patrol', 'seasonal_race', 'placement', 'get_total_score')


admin.site.register(PointSet, PointSetAdmin)
admin.site.register(Point, PointAdmin)
