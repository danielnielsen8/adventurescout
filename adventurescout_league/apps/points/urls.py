# -*- coding: utf-8
from django.conf.urls import url
from adventurescout_league.apps.points.views import PointOverview, PointSetOverview

urlpatterns = [
    url(r'^$', PointOverview.as_view(), name="point_overview"),
    url(r'^season/(?P<season_num>\d+)/$', PointSetOverview.as_view(), name="point_set_season"),
]
