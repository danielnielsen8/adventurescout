from django.views.generic.base import TemplateView
from django.http import Http404

from .models import PointSet


class PointOverview(TemplateView):
    template_name = 'points/overview.html'

    def get_context_data(self, **kwargs):
        context = super(PointOverview, self).get_context_data(**kwargs)

        # Current point set
        point_set_current = PointSet.objects.current()
        if point_set_current:
            if point_set_current.published or self.request.user.is_staff:
                context['point_set_current'] = point_set_current

        # Archived point sets
        point_set_archived = [ps for ps in PointSet.objects.archived() if ps.published or self.request.user.is_staff]
        context['point_set_archived'] = point_set_archived

        return context


class PointSetOverview(TemplateView):
    template_name = 'points/point_set_overview.html'

    def get_context_data(self, **kwargs):
        context = super(PointSetOverview, self).get_context_data(**kwargs)
        try:
            point_set = PointSet.objects.get(season__number=kwargs['season_num'])
        except PointSet.DoesNotExist:
            raise Http404

        context.update({
            'mode': point_set.mode,
            'point_set': point_set,
        })
        return context
