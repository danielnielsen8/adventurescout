import django.forms as forms
from adventurescout_league.apps.league.models import CompetitionClass
from adventurescout_league.apps.races.models import SeasonalRace
from adventurescout_league.apps.utils.widgets import DateTimePicker


class SeasonalRaceForm(forms.ModelForm):
    """
    Custimized form for creating/updating seasonal patrols.

    - m2m field: 'competition_classes' is translated into to individual 
      model choice fields: 'age_class' and 'comp_class'
      on save and instantiation
    - foreignKey related field: 'members' is changed to checkbox widget
    """
    class Meta:
        model = SeasonalRace
        exclude = ('race', 'season', 'competition_classes')
        widgets = {
            'event_start': DateTimePicker(),
            'event_end': DateTimePicker()
        }

    age_class = forms.ModelMultipleChoiceField(
        queryset=CompetitionClass.objects.filter(category='age'),
        widget=forms.CheckboxSelectMultiple)
    comp_class = forms.ModelMultipleChoiceField(
        queryset=CompetitionClass.objects.filter(category='competition'),
        widget=forms.CheckboxSelectMultiple)

    def __init__(self, *args, **kwargs):
        """
        Populate custom fields with data from model instance
        """
        super(SeasonalRaceForm, self).__init__(*args, **kwargs)
        if kwargs['instance']:
            inst = kwargs['instance']
            try:
                pass
                self.fields['age_class'].initial = inst.competition_classes.filter(category='age')
                self.fields['comp_class'].initial = inst.competition_classes.filter(category='competition')
            except Exception:
                pass

    def save(self, commit=True):
        """
        Save data from custom fields to model instance
        """
        self.cleaned_data['competition_classes'] = self.cleaned_data['age_class'] | \
                                                   self.cleaned_data['comp_class']
        return super(SeasonalRaceForm, self).save(commit=commit)