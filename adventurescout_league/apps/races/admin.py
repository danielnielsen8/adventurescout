# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.races.models import Race, SeasonalRace


class RaceAdmin(admin.ModelAdmin):
    list_display = ('name', 'created', 'get_author')
    search_fields = ('name',)

    def get_author(self, obj):
        return obj.owner.email

    get_author.short_description = 'Ejer'


class SeasonalRaceAdmin(admin.ModelAdmin):
    list_display = ('get_league_id', 'race', 'event_start', 'event_end',)
    search_fields = ('race',)


admin.site.register(Race, RaceAdmin)
admin.site.register(SeasonalRace, SeasonalRaceAdmin)