import django_tables2 as tables
from django.core.urlresolvers import reverse
from django.utils.safestring import SafeText
from django.utils.translation import ugettext as _

from adventurescout_league.apps.races.models import *


class RaceTable(tables.Table):
    class Meta:
        model = SeasonalRace
        fields = ('league_id', 'race', 'class_age', 'class_competition',)

    league_id = tables.Column(accessor='get_league_id', order_by=('id'), 
                              verbose_name='LigaID', attrs={"td": {"class": "monospaced"}})
    race = tables.LinkColumn('race_detail', args=[tables.utils.A('race.pk')], verbose_name=_('Race'))

    class_age = tables.Column(accessor='get_competition_classes', orderable=False,
                              verbose_name=_('Age class'))
    class_competition = tables.Column(accessor='get_competition_classes', orderable=False,
                                      verbose_name=_('Competition class'))

    def render_class_age(self, value):
        """
        Return age classes as nice string
        """
        s = ', '.join([c.name for c in value if c.category == 'age'])
        return s if s else _('None')

    def render_class_competition(self, value):
        """
        Return competition classes as nice string
        """        
        s = ', '.join([c.name for c in value if c.category == 'competition'])
        return s if s else _('None')


class UserSeasonalRacesTable(tables.Table):
    """
    Table showing Seasonal races belonging to an user 
    """
    season = tables.Column()
    seasonal_race = tables.Column()
    status = tables.Column()
    actions = tables.Column(
        orderable=False,
        attrs={
            "td": {"class": "align-right"},
            "th": {"class": "align-right"}
        }
    )

    def render_actions(self, value):
        if value:
            if value['op'] == 'create':
                label = _('create')
                link = reverse('seasonal_race_create', kwargs={'season_num': value['season_num']})
                html_class = ('button-small', 'red')
            elif value['op'] == 'edit':
                label = _('edit')
                link = reverse('seasonal_race_edit', kwargs={'pk': value['pk']})
                html_class = ('button-small',)

            return SafeText('<a class="{}" href="{}">{}</a>'.format(' '.join(html_class), link, label))
        return '-'
