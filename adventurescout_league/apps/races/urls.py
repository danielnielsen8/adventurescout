# -*- coding: utf-8
from django.conf.urls import url

from adventurescout_league.apps.races.views import (
    RaceCreateView, RaceDetailView, RaceListView, RaceUpdateView,
    SeasonalRaceCreateView, SeasonalRaceUpdateView,
)

urlpatterns = [

    url(r'^list/$', RaceListView.as_view(), name="race-list"),
    # Detail
    url(r'^detail/(?P<pk>\d+)/$', RaceDetailView.as_view(), name="race_detail"),
    # Get by leagueID
    url(r'^(?P<league_id>R\d{2}\w{2}\d{3})/$', RaceDetailView.as_view(), name="race_detail_by_league_id"),    

    url(r'^create/$', RaceCreateView.as_view(), name="race_create"),
    url(r'^edit/(?P<pk>\d+)/$', RaceUpdateView.as_view(), name="race_edit"),

    # Seasonal Races
    url(r'^seasonal/(?P<season_num>\d+)/create/$', SeasonalRaceCreateView.as_view(), name='seasonal_race_create'),
    url(r'^seasonal/edit/(?P<pk>\d+)/$', SeasonalRaceUpdateView.as_view(), name='seasonal_race_edit')
]
