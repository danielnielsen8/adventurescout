# -*- coding: utf-8
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView, UpdateView
from django.forms.models import modelform_factory

from adventurescout_league.apps.league.views import *  # noqa
from adventurescout_league.apps.races.forms import *  # noqa
from adventurescout_league.apps.races.tables import *  # noqa


class UserRaceDoesNotExist(Exception):
    msg = 'No Race has been created for this user'

# ------------------------------------------------------- #
# Base views                                              #
# ------------------------------------------------------- #


class RaceBaseFormMixin(LoginRequiredMixin, PermissionRequiredMixin):
    """
    Base Race form mixin
    """
    extra_context = None

    def get_success_url(self):
        return reverse('accounts_dashboard')

    def get_context_data(self, **kwargs):
        context = super(RaceBaseFormMixin, self).get_context_data(**kwargs)
        context.update(self.extra_context)
        return context

    def get_user_race(self):
        try:
            race = Race.objects.get(owner=self.request.user)
        except Race.DoesNotExist:
            raise UserRaceDoesNotExist
        return race

    def get_current_season(self):
        return Season.get_current_season()


class RaceBaseCreateView(RaceBaseFormMixin, CreateView):
    pass


class RaceBaseUpdateView(RaceBaseFormMixin, UpdateView):

    def form_valid(self, form):
        self.object = self.get_object()
        form.save(self.object)

        return HttpResponseRedirect(self.get_success_url())

# ------------------------------------------------------- #
# List views                                              #
# ------------------------------------------------------- #


class RaceListView(LeagueListView):
    model = SeasonalRace
    table_class = RaceTable
    paginate_by = 18
    thumb_item_template_name = 'races/race_list_item.html'

    extra_context = {
        'page_headline': 'Løbsoversigt',
        'page_description': 'Oversigt over løbenes sæsontilmeldinger til ligaen'
    }

    allowed_filters = {
        'class_age': 'competition_classes__identifier__iexact',
        'class_competition': 'competition_classes__identifier__iexact',
        'season': 'season__number',
        'search': 'race__name__icontains'
    }


class RaceDetailView(LeagueDetailView):
    model = Race
    context_object_name = 'race'
    template_name = 'races/race_detail.html'

    def get_object(self, **kwargs):
        # Get the patrol either by
        # direct 'pk' or through
        # the 'league id'
        if 'pk' in self.kwargs:
            
            # Test hack
            if self.kwargs['pk'] == '12' and not self.request.user.is_superuser:
                raise PermissionDenied
            
            return get_object_or_404(self.model, pk=self.kwargs['pk'])
        elif 'league_id' in self.kwargs:
            seasonal_race = SeasonalRace.get_object(league_id=self.kwargs['league_id'])
            return seasonal_race.race


# ------------------------------------------------------- #
# Races                                                   #
# ------------------------------------------------------- #
    
class RaceCreateView(RaceBaseCreateView):
    model = Race
    extra_context = {
        'page_title': _('Create Race')
    }
    required_perms = ('races.add_race',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('owner',))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


class RaceUpdateView(RaceBaseUpdateView):
    model = Race
    extra_context = {
        'page_title': _('Update Race')
    }
    required_perms = ('races.change_race',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('owner',))

# ------------------------------------------------------- #
# Seasonal Races                                          #
# ------------------------------------------------------- #


class SeasonalRaceCreateView(RaceBaseCreateView):
    model = SeasonalRace
    extra_context = {
        'page_title': _('Create seasonal race')
    }
    required_perms = ('races.add_seasonalrace',)

    def dispatch(self, request, *args, **kwargs):
        self.season = Season.objects.get(number=kwargs['season_num'])
        return super(SeasonalRaceCreateView, self).dispatch(request, *args, **kwargs)

    def get_form_class(self):
        return SeasonalRaceForm

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.race = self.get_user_race()
        self.object.season = self.season
        self.object.save()
        form.save_m2m()

        return HttpResponseRedirect(self.get_success_url())


class SeasonalRaceUpdateView(RaceBaseUpdateView):
    model = SeasonalRace
    extra_context = {
        'page_title': _('Update Seasonal Race')
    }
    required_perms = ('races.change_seasonalrace',)

    def get_form_class(self):
        return SeasonalRaceForm
