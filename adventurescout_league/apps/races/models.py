# -*- coding: utf-8

from __future__ import unicode_literals

import re
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User
from adventurescout_league.apps.league.models import Season, SeasonalBase


# A scout race
# - Basic / static infomation about the race
@python_2_unicode_compatible
class Race(models.Model):

    class Meta:
        verbose_name = _('Race')
        verbose_name_plural = _('Races')

    # Fields
    name = models.CharField(max_length=256, verbose_name=_('Name'))
    description = models.TextField(verbose_name=_('Description'))

    cover_image = models.ImageField(upload_to='races', blank=True, null=True)
    list_image = models.ImageField(upload_to='races', blank=True, null=True)
    list_logo = models.ImageField(upload_to='races', blank=True, null=True)

    created = models.DateField(auto_now_add=True, verbose_name=_('created'))
    owner = models.ForeignKey(User, related_name='race', verbose_name=_('Owner'), on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    @cached_property
    def seasonal_list(self):
        """
        Returns a list of all seasonal races
        belonging to this Patrol
        """
        if hasattr(self, '_seasonal_list'):
            self._seasonal_list = SeasonalRace.objects.filter(patrol=self)
            yield self._seasonal_list

    def get_seasonal(self, season):
        """
        Returns the Seasonal race with season = *season*
        """
        seasonal = [obj for obj in self.seasonal_list if obj.season == season]
        if len(seasonal) == 1:
            return seasonal[0]
        else:
            return None

    def detail_list(self, fields=None):
        """
        Return a list of tuples showing relevant Information
        about this object - leaving out unimportant fields
        """
        allowed_fields = fields if fields else ('name', 'description',)
        detail_list = []

        for field in allowed_fields:
            try:
                detail_list.append((field, self.__dict__[field],))
            except KeyError:
                pass
        return tuple(detail_list)


# A seasonal race
# - Information about a race's annual subscription (to the league)
# to a season
# - Inherits from the Seasonal Base in the league app
@python_2_unicode_compatible
class SeasonalRace(SeasonalBase):

    class Meta:
        verbose_name = _('Seasonal Race')
        verbose_name_plural = _('Seasonal Race')
        unique_together = ('race', 'season')

    # Fields
    event_start = models.DateTimeField(verbose_name=_('Event start'))
    event_end = models.DateTimeField(verbose_name=_('Event end'))

    race = models.ForeignKey(
        Race, related_name='seasonal_races', verbose_name=_('Race'),
        on_delete=models.CASCADE
    )
    season = models.ForeignKey(
        Season, related_name='seasonal_races', verbose_name=_('Season'),
        on_delete=models.CASCADE
    )

    def __str__(self):
        return '{1:s} : {0:s}'.format(self.race.name, self.get_league_id())

    def get_league_id(self):
        kls_age = [kls.identifier for kls in self.get_competition_classes() if kls.category == 'age']
        kls_comp = [kls.identifier for kls in self.get_competition_classes() if kls.category == 'competition']

        class_age = kls_age[0] if len(kls_age) == 1 else 'A' if len(kls_age) > 1 else '?'
        class_comp = kls_comp[0] if len(kls_comp) == 1 else 'A' if len(kls_comp) > 1 else '?'

        return 'R{:02d}{}{}{:03d}'.format(self.season.number, class_age, class_comp, self.serial_num)

    def detail_list(self):
        return (
            (_('LeagueID'), self.get_league_id(),),
            (_('Classes'), self.competition_classes,),
            (_('Event start'), self.event_start,),
            (_('Event end'), self.event_end,)
        )

    @classmethod
    def get_object(cls, league_id=''):
        if league_id:
            m = re.match(r'^R(?P<season_num>\d{2})\w{2}(?P<serial_num>\d{3})$', league_id)
            if m:
                d = m.groupdict()
                return cls.objects.get(season__number=d['season_num'],
                                       serial_num=d['serial_num'])
            else:
                raise cls.DoesNotExist
