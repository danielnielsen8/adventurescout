from adventurescout_league.apps.league.serializers import *
from adventurescout_league.apps.races.models import Race


class RaceSerializer(serializers.ModelSerializer):
    class Meta:
        model = Race
        exclude = ('id', 'owner',)


class SeasonalRaceSerializer(serializers.Serializer):
    league_id = serializers.CharField(source='get_league_id', read_only=True, required=False)
    created = serializers.DateField(required=False)
    season = SeasonSerializer(required=False)
    race = RaceSerializer()
    event_start = serializers.CharField()
    event_end = serializers.CharField()
    competition_classes = CompetitionClassIdentifierSerializer(many=True)