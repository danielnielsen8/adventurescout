# -*- coding: utf-8

from __future__ import unicode_literals

from django import template
from django.utils.translation import ugettext as _
from adventurescout_league.apps.races.tables import *
from adventurescout_league.apps.league.models import Season

register = template.Library()


@register.inclusion_tag('races/includes/race_current_seasonal.html')
def race_current_seasonal(race):
    seasons = Season.objects.all()
    seasonal_races = []
    season = None
    for season in seasons:
        try:
            seasonal_race = SeasonalRace.objects.get(race=race, season=season)
            seasonal_races.append(seasonal_race)
        except SeasonalRace.DoesNotExist:
            continue
    return {'seasonal_races': seasonal_races, 'season': season}


@register.inclusion_tag('races/includes/user_race.html')
def user_race(user, request):
    """
    Show (private) race for user
    """
    try:
        race = Race.objects.get(owner=user)
    except Race.DoesNotExist:
        race = False
    except Race.MultipleObjectsReturned:
        race = False
    
    return {'race': race, 'request': request}


@register.inclusion_tag('races/includes/user_seasonal_races.html')
def user_seasonal_races(user, request):
    """
    Show seasonal races associated with user
    or help text
    """
    context = {}
    try:
        race = Race.objects.get(owner=user)
    except Race.DoesNotExist:
        race = None
        context['action_needed'] = u'Det er nødvendigt at oprette et løb, før du kan oprette sæsonløb!'
    except Race.MultipleObjectsReturned:
        race = None
        context['action_needed'] = u'Du har oprettet mere end et løb! Hm..'

    if race:
        seasons = Season.objects.all()
        data = []

        for season in seasons:
            try:
                seasonal_race = SeasonalRace.objects.get(season=season, race=race)
            except SeasonalRace.DoesNotExist:
                seasonal_race = None
            except SeasonalRace.MultipleObjectsReturned:
                seasonal_race = None
                context['action_needed'] = (
                    u'Det er sket en fejl! Du har mere end et sæsonløb for season: {}'.format(season)
                )

            if seasonal_race:
                if season.is_active():
                    status = _('Active')
                    context['current_seasonal_race_exists'] = True
                else:
                    status = _('Inactive')
            else:
                status = _('Not created')

            # Determine actions
            if seasonal_race:
                actions = {
                    'op': 'edit',
                    'pk': seasonal_race.pk
                }
            else:
                actions = {
                    'op': 'create',
                    'season_num': season.number
                }

            data.append({
                'season': season.name,
                'seasonal_race': seasonal_race,
                'status': status,
                'actions': actions
                })
        context['seasonalrace_table'] = UserSeasonalRacesTable(data)
    return context
