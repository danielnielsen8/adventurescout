from django.utils.safestring import SafeText, SafeData
from django.utils.translation import ugettext as _
from django_tables2.templatetags.django_tables2 import title
import django_tables2 as tables


class ControlColumn(tables.Column):
    def __init__(self, *args, **kwargs):
        if 'name' in kwargs:
            self.name = kwargs['name']
            del kwargs['name']        
        super(ControlColumn, self).__init__(*args, **kwargs)

    def render(self, value):
        return SafeText('<input type="checkbox" name="{}" value="{}">'.format(self.name, value) +
                        '&nbsp;<label>{}</label>'.format(_('Yes')))

    @property
    def header(self):
        verbose_name = self.verbose_name
        if isinstance(verbose_name, SafeData):
            return verbose_name
        return title(verbose_name)
