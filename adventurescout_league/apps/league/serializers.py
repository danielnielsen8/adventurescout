# coding: utf-8

from __future__ import unicode_literals

from rest_framework import serializers
from adventurescout_league.apps.league.models import Season


class SeasonSerializer(serializers.ModelSerializer):
    unicode_str = serializers.CharField(source='__str__', read_only=True)

    class Meta:
        model = Season
        exclude = ('id',)


class CompetitionClassSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)
    identifier = serializers.CharField()
    category = serializers.CharField(required=False)
    unicode_str = serializers.CharField(source='__str__')


class CompetitionClassIdentifierSerializer(serializers.Serializer):
    name = serializers.CharField(required=False)
    identifier = serializers.CharField()
    category = serializers.CharField(required=False)
