# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.league.models import Season, CompetitionClass


class SeasonAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'start_date', 'end_date', 'is_active', 'registration_is_open')
    ordering = ['-start_date']


class CompetitionClassAdmin(admin.ModelAdmin):
    pass


admin.site.register(Season, SeasonAdmin)
admin.site.register(CompetitionClass, CompetitionClassAdmin)