# -*- coding: utf-8
from django import forms
from django.utils.translation import ugettext as _
from adventurescout_league.apps.league.models import Season, SeasonalBase, CompetitionClass

LIST_STYLES = (
    ('table', 'Table',),
    ('thumbs', 'Thumbs',),
)


class RequestFormMixin(object):
    """
    Form Request Mixin
    Allow 'request' to be included in kwargs
    """
    def __init__(self, *args, **kwargs):
        if 'request' in kwargs:
            self.request = kwargs['request']
            del kwargs['request']
        super(RequestFormMixin, self).__init__(*args, **kwargs)


# ChoiceField with optional 'empty' option
class EmptyChoiceField(forms.ChoiceField):
    def __init__(self, choices=(), empty_label=None, required=True, widget=None, label=None,
                 initial=None, help_text=None, *args, **kwargs):

        # prepend an empty label if it exists (and field is not required!)
        if not required and empty_label is not None:
            choices = tuple([(u'', empty_label)] + list(choices))

        super(EmptyChoiceField, self).__init__(choices=choices, required=required, widget=widget, label=label,
                                               initial=initial, help_text=help_text, *args, **kwargs)


class LeagueListFilterForm(forms.Form):
    search = forms.CharField(required=False, label=_('Search'), initial='')
    season = forms.ChoiceField(
        required=False, choices=Season.get_season_choices(),
        label=_('Season')
    )
    class_age = EmptyChoiceField(
        required=False, empty_label=_('All'),
        choices=CompetitionClass.get_choices_by_category('age'),
        label=_('Age class'),
    )
    class_competition = EmptyChoiceField(
        required=False, empty_label=_('All'),
        choices=CompetitionClass.get_choices_by_category('competition'), label=_('Competition class')
    )
    list_type = forms.ChoiceField(
        choices=LIST_STYLES, widget=forms.RadioSelect,
        label=_('Display'),
    )
