# -*- coding: utf-8
from django.core.exceptions import ImproperlyConfigured, PermissionDenied
from django.shortcuts import redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic import TemplateView
from django_tables2.config import RequestConfig
from adventurescout_league.apps.league.forms import LeagueListFilterForm


class FrontView(TemplateView):
    def dispatch(self, request, **kwargs):
        return redirect('/forside/')


# Doc needed
class FilterMixin(object):
    allowed_filters = {}

    def get_queryset_filters(self):
        filters = {}
        for item in self.allowed_filters:
            # Ignore empty values
            if item in self.request.GET and self.request.GET[item]:
                filters[self.allowed_filters[item]] = self.request.GET[item]
        return filters

    def get_queryset(self):
        return super(FilterMixin, self).get_queryset().filter(**self.get_queryset_filters())


class LeagueDetailView(DetailView):
    template_name = 'base/base_detail.html'


class LeagueListView(FilterMixin, ListView):
    """
    League List view
    Presents a list of league entities (Race, Patrol, SeasonalPatrol etc.)
    'forked' version of 'SingleTableMixin' fra django_tables2
    """

    table_class = None
    table_data = None
    context_table_name = None
    table_pagination = None
    template_name = 'base/base_list.html'
    thumb_item_template_name = ''
    filter_form = LeagueListFilterForm

    # Only affects thumb list
    paginate_by = 12

    # Extra context used in the template
    extra_context = {
        'page_headline': '',
        'page_description': ''
    }

    # Allowed filters
    allowed_filters = {}

    initial_filter_values = {
        'season': 8,
        'list_type': 'table',
        'search': ''
    }

    # Set initial filtering values, like the season etc.
    def get_queryset(self):
        objects = super(LeagueListView, self).get_queryset()

        # Prefetch related 'Competition classes'
        objects = objects.prefetch_related('competition_classes')

        if not self.request.GET:
            objects = objects.filter(season__number=7)

        return objects

    def get_context_data(self, **kwargs):
        context = super(LeagueListView, self).get_context_data(**kwargs)
        params = self.request.GET.copy()

        # Initial filter form values
        for key, value in self.initial_filter_values.items():
            if key not in params:
                params[key] = value

        context['thumb_template'] = self.thumb_item_template_name
        context['filter_form'] = self.filter_form(params)
        context['list_type'] = params['list_type']

        # Add extra context
        context.update(self.extra_context)

        # Add table
        table = self.get_table()
        context[self.get_context_table_name(table)] = table

        return context

    def get_table(self):
        """
        Return a table object to use. The table has automatic support for
        sorting and pagination.
        """
        options = {}
        table_class = self.get_table_class()
        table = table_class(self.get_table_data())
        paginate = self.get_table_pagination()  # pylint: disable=E1102
        if paginate is not None:
            options['paginate'] = paginate
        RequestConfig(self.request, **options).configure(table)
        return table

    def get_table_class(self):
        """
        Return the class to use for the table.
        """
        if self.table_class:
            return self.table_class
        raise ImproperlyConfigured(u"A table class was not specified. Define "
                                   u"%(cls)s.table_class"
                                   % {"cls": type(self).__name__})

    def get_context_table_name(self, table):
        """
        Get the name to use for the table's template variable.
        """
        return self.context_table_name or "table"

    def get_table_data(self):
        """
        Return the table data that should be used to populate the rows.
        """
        if self.table_data:
            return self.table_data
        elif hasattr(self, "get_queryset"):
            return self.get_queryset()
        raise ImproperlyConfigured(u"Table data was not specified. Define "
                                   u"%(cls)s.table_data"
                                   % {"cls": type(self).__name__})

    def get_table_pagination(self):
        """
        Returns pagination options: True for standard pagination (default),
        False for no pagination, and a dictionary for custom pagination.
        """
        return self.table_pagination
