from django import template
from django.forms import widgets
from django.utils.html import format_html

register = template.Library()


@register.simple_tag
def field_css_class(field):
    css_class = 'unknown'
    widget = field.field.widget

    if type(widget) is widgets.TextInput:
        css_class = 'text'
    elif type(widget) is widgets.Textarea:
        css_class = 'textarea'
    elif type(widget) is widgets.PasswordInput:
        css_class = 'password'
    elif type(widget) is widgets.Select:
        css_class = 'select'
    elif type(widget) is widgets.RadioSelect:
        css_class = 'radio'
    elif type(widget) is widgets.CheckboxSelectMultiple:
        css_class = 'checkbox'
    elif type(widget) is widgets.DateInput:
        css_class = 'date'
    elif type(widget) is widgets.DateTimeInput:
        css_class = 'datetime'
    elif type(widget) is widgets.ClearableFileInput:
        css_class = 'file'        

    return '{}-field'.format(css_class)


@register.simple_tag
def form_general_errors(form):
    try: 
        return format_html(u'<div class="alert">{}</div>'.format(form.errors['__all__']))
    except KeyError:
        return ''