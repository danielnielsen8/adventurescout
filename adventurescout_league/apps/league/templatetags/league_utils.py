# -*- coding: utf-8
from django.db.models import Manager
from django import template

register = template.Library()


@register.inclusion_tag('league/includes/pprint_object.html')
def pprint_object(obj):
    if hasattr(obj, 'detail_list'):
        detail_list = []
        for item in obj.detail_list():
            # Ducktyping (Manager)
            label, value = item
            if isinstance(value, Manager):
                value = u', '.join([str(obj) for obj in value.all()])

            detail_list.append((label, value,))
                
    else:
        detail_list = []
    return {'detail_list': detail_list}


@register.inclusion_tag('league/includes/tabs.html')
def build_tabs(tabs_content, user, request):
    print(tabs_content)
    content = []

    # Filter out items which are not allowed for the user
    for item in tabs_content:
        try:
            if not user.has_perms(item['perms']):
                continue
        except KeyError:
            pass
        content.append(item)
    return {'content': content, 'user': user, 'request': request}
