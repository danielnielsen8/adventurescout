
from datetime import date
import damm
from adventurescout_league.apps.league.models import Season

class LeagueSeasonError(Exception):
    msg = 'Only one active season may exist. Zero or multiple active seasons where found'

    def __init__(self, msg=''):
        if msg is not '':
            self.msg = msg
    def __str__(self):
        return repr(self.msg)

# Get current active season
# - return the latest active season (start_date < today < end_date)
#   tries to find only one current season - fails otherwise
def get_current_season():
    active_seasons = Season.objects.filter(
        start_date__lte=date.today(),
        end_date__gte=date.today()
    )

    if len(active_seasons) == 1:
        return active_seasons[0]
    elif len(active_seasons) == 0:
        raise LeagueSeasonError()
    else:
        raise LeagueSeasonError()