import damm

# Damm stuff
def damm_check(code):
    return damm.check(code)

def damm_append_digit(num):
    return int(str(num) + str(damm.encode(num)))

def damm_extract(code):
    return int(str(code)[:-1])