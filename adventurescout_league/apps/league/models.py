# -*- coding: utf-8

from __future__ import unicode_literals

from datetime import date
from django.db import models
from django.db.models import Max, Index
from django.utils.encoding import python_2_unicode_compatible
from django.utils.translation import ugettext as _
from adventurescout_league.apps.league.check_code import damm_append_digit


class LeagueSeasonError(Exception):
    msg = 'Only one active season may exist. Zero or multiple active seasons where found'

    def __init__(self, msg=''):
        if msg is not '':
            self.msg = msg

    def __str__(self):
        return repr(self.msg)


# A Season
# - Information about the annual season
@python_2_unicode_compatible
class Season(models.Model):
    ACTIVE = _('active')
    HELD = _('held')
    COMING = _('coming')

    class Meta:
        verbose_name = _('Season')
        verbose_name_plural = _('Seasons')
        indexes = [
            models.Index(fields=['number']),
            models.Index(fields=['-start_date']),
            models.Index(fields=['start_date', 'end_date']),
            models.Index(fields=['registration_open', 'registration_close']),
        ]

    # Fields
    number = models.PositiveIntegerField(verbose_name=_('Number'), unique=True)
    name = models.CharField(max_length=256, verbose_name=_('Name'))

    start_date = models.DateField(verbose_name=_('Start date'))
    end_date = models.DateField(verbose_name=_('End date'))

    registration_open = models.DateField(verbose_name=_('Registration open'))
    registration_close = models.DateField(verbose_name=_('Registration close'))

    def __str__(self):
        return _('Season {:02d} : {:s}').format(self.number, self.name)

    def is_active(self):
        return self.start_date <= date.today() <= self.end_date

    def registration_is_open(self):
        return self.registration_open <= date.today() <= self.registration_close

    def use_new_league_id(self):
        return self.number > 4

    def get_status(self):
        today = date.today()

        if today > self.end_date:
            return self.HELD
        elif self.is_active():
            return self.ACTIVE
        elif today < self.start_date:
            return self.COMING

        return ''

    # Get a choice tuple of seasons
    # for use in models and forms
    @classmethod
    def get_season_choices(cls):
        return tuple(
            [tuple([season.number, season]) for season in cls.objects.order_by('-start_date').all()]
        )

    # Get current active season
    # - return the latest active season (start_date < today < end_date)
    #   tries to find only one current season - fails otherwise
    @classmethod
    def get_current_season(cls):
        active_seasons = cls.objects.filter(
            start_date__lte=date.today(), end_date__gte=date.today()
        )

        if len(active_seasons) == 1:
            return active_seasons[0]
        elif len(active_seasons) == 0:
            raise LeagueSeasonError()
        else:
            raise LeagueSeasonError()


@python_2_unicode_compatible
class CompetitionClass(models.Model):
    CATEGORIES = (
        ('age', _('Age classes'),),
        ('competition', _('Competition classes'),),
    )

    name = models.CharField(max_length=100)
    identifier = models.CharField(max_length=1, unique=True)
    category = models.CharField(max_length=100, choices=CATEGORIES)

    def __str__(self):
        return '{} : {}'.format(self.identifier, self.name)

    @classmethod
    def get_choices_by_category(cls, category):
        obj_list = cls.objects.filter(category=category)
        return [[obj.identifier, obj.name] for obj in obj_list]

    class Meta:
        indexes = [Index(fields=['category'])]


class SeasonalBase(models.Model):
    """
    A base class for seasonal objects in the league
    """

    class Meta:
        abstract = True

    # Fields
    competition_classes = models.ManyToManyField(CompetitionClass, blank=True)
    serial_num = models.PositiveIntegerField(editable=False, verbose_name=_('Serial number'))
    created = models.DateField(auto_now_add=True, verbose_name=_('Created'))

    # Models save override
    def save(self, *args, **kwargs):
        # Auto set serial number to a (within season) unique value
        if not self.serial_num:
            self.serial_num = self.make_serial_num(self.season)
        super(SeasonalBase, self).save(*args, **kwargs)

    def get_code_num(self):
        return damm_append_digit(self.serial_num)

    def get_competition_classes(self, category=None):
        """
        Get Competition Classes
        Return list of Competition Classes
        read from m2m field: competition_class

        avoid using SeasonalBase.competition_classes directly
        avoid using 'filter' on manager

        TODO: Impl. some sort of caching
        """
        return self.competition_classes.all()

    def get_competition_classes_sorted(self):
        """
        Returns of sorted list of competition classes
        Mostly useful for templates
        """
        sorted_list = []
        for c in CompetitionClass.CATEGORIES:
            sorted_list.append({
                'category': c,
                'list': [kls for kls in self.get_competition_classes() if kls.category == c[0]]
            })
        return sorted_list

    def has_comp_class(self, comp_class):
        """
        Checks if the seasonal patrol is registrered for
        certain instance of *CompetitionClass*
        """
        if hasattr(self, '_comp_class_idents'):
            return comp_class.identifier in self._comp_class_idents
        else:
            self._comp_class_idents = self.competition_classes.values_list('identifier', flat=True)
            return self.has_comp_class(comp_class)

    def has_comp_classes(self, comp_classes):
        """
        Checks if the seasonal patrol is registrered for
        list of instances of *CompetitionClass*
        """
        for comp_class in comp_classes:
            if not self.has_comp_class(comp_class):
                return False
        return True

    def get_league_id(self):
        """
        Get League ID
        Should be impl. in subclasses!
        It won't raise any error though - but simply returns 'N/A'
        """
        return 'N/A'

    def get_comp_classes(self):
        """
        Caches list of competition classes
        """
        if hasattr(self, '_comp_classes'):
            return self._comp_classes
        else:
            self._comp_classes = list(self.competition_classes.all())
            return self._comp_classes

    # Get next available serial number for specific season
    @classmethod
    def make_serial_num(cls, season):
        from adventurescout_league.apps.league.utils import get_current_season

        serial_num_agg = cls.objects.filter(season=season).aggregate(Max('serial_num'))
        serial_num_max = serial_num_agg['serial_num__max']

        # If aggregation fails, no objects has been created and we should return 1
        if serial_num_max is not None:
            return serial_num_max + 1
        else:
            return 1
