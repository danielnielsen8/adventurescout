from django.conf import settings
from adventurescout_league.apps.league.models import Season


def extra_context(request):
    return {
        'SITE_URL': settings.SITE_URL,
        'CURRENT_SEASON': Season.get_current_season()
    }
