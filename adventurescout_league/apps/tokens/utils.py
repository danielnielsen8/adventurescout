from datetime import timedelta

from rest_framework import serializers

from .models import Token

ONE_DAY = timedelta(1, 0)


def create_serializer_token(token_type, serializer, lifetime=ONE_DAY):
    """
    Create a token containing the data for the seasonal patrol
    and return the token
    """
    assert isinstance(serializer, serializers.Serializer)

    token = Token.objects.create(token_type, serializer.data, lifetime)
    token.save()

    return token