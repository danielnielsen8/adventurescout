# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.tokens.models import Token

class TokenAdmin(admin.ModelAdmin):
	pass

admin.site.register(Token, TokenAdmin)