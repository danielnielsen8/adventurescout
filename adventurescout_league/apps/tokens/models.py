# coding: utf-8

from __future__ import unicode_literals

from datetime import datetime, timedelta

from django.contrib.postgres.fields import JSONField
from django.db import models
from django.utils.six import python_2_unicode_compatible
from django.utils.timezone import utc

from .fields import UUIDField


class TokenManager(models.Manager):
    def create(self, token_type, data, lifetime=None):
        assert type(lifetime) is None or timedelta

        obj = self.model()
        obj.token_type = token_type
        obj.data = data
        if lifetime is not None:
            obj.valid_to = datetime.utcnow().replace(tzinfo=utc) + lifetime
        return obj


@python_2_unicode_compatible
class Token(models.Model):
    objects = TokenManager()

    code = UUIDField()
    valid_to = models.DateTimeField(null=True, blank=True)
    token_type = models.CharField(max_length=40)
    data = JSONField()

    @property
    def full_code(self):
        return str(self.id) + '-' + self.code

    def __str__(self):
        return '%s - %s' % (self.token_type, self.full_code)

    @property
    def has_expired(self):
        return self.valid_to < datetime.utcnow().replace(tzinfo=utc)
