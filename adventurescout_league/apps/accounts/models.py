# -*- coding: utf-8

from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models
from django.contrib.auth import get_user_model
from django.http import Http404
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _

from adventurescout_league.apps.races.models import Race
from adventurescout_league.apps.patrols.models import Patrol
from adventurescout_league.apps.tokens import register


# Register tokens
@register('account_token')
def account_token_handler(request, token):
    # If token has expired raise '404 Not Found'
    if token.has_expired:
        raise Http404
    return token.data


class TooManySeasonalsError(Exception):
    pass


class LeagueUser(User):
    """
    ## Proxy model for django.contrib.auth User

    adds various helpful methods for retrieving patrols 
    and seasonal patrols
    """
    class Meta:
        proxy = True

    @cached_property
    def patrol(self):
        """
        Get users patrol or None
        """
        try:
            return self._patrol
        except AttributeError:
            try:
                self._patrol = Patrol.objects.get(owner=self)
            except Patrol.DoesNotExist:
                self._patrol = None
            return self.patrol

    @cached_property
    def race_list(self):
        """
        Get users race list or None
        """
        try:
            return self._race_list
        except AttributeError:
            self._race_list = Race.objects.filter(owner=self)
            return self.race_list


@python_2_unicode_compatible
class LeagueProfile(models.Model):
    """
    Profile for league user
    used to hold personal information
    """
    SEX_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female')),
    )

    full_name = models.CharField(max_length=128)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES)
    address = models.CharField(max_length=128)
    postal_code = models.PositiveIntegerField()
    city = models.CharField(max_length=128)
    phone = models.CharField(max_length=128)
    birth_date = models.DateField()
    allow_notifications = models.BooleanField()

    user = models.OneToOneField(
        get_user_model(), blank=True, null=True,
        verbose_name=_('user'), related_name='profile', unique=True, on_delete=models.CASCADE
    )

    # Labels (in danish for now!)
    full_name.verbose_name = u'Rigtige navn'
    full_name.help_text = u'Indtast dit fulde navn, med fornavn og efternavn'
    sex.verbose_name = u'Køn'
    sex.help_text = u'Vælg dit køn. Dette vil blive brugt til at vurdere jeres konkurrenceklasse'
    address.verbose_name = u'Adresse'
    address.help_text = u'Indtast din adresse (Gadenavn og husnummer)'
    postal_code.verbose_name = u'Postnummer'
    postal_code.help_text = u'Indtast dit postnummer (4 cifre)'
    city.verbose_name = u'By'
    city.help_text = u'Indtast bynavn'
    phone.verbose_name = u'Telefonnummer'
    phone.help_text = u'Indtast dit telefonnummer (mobilnummer)'
    birth_date.verbose_name = u'Fødselsdato'
    birth_date.help_text = u'Indtast din fødselsdato i formattet dd.mm.åååå. fx. 19.02.1990'
    allow_notifications.verbose_name = u'Tillad meddelelser'
    allow_notifications.help_text = u'Ønsker du at modtage emails om nye tiltag, eller ændringer i ligaen?'

    def __str__(self):
        return '{} ({})'.format(self.full_name, self.user.email)

