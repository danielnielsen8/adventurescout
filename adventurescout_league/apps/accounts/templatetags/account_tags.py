# -*- coding: utf-8

from __future__ import unicode_literals

from django import template
from django.core.urlresolvers import reverse

register = template.Library()


@register.inclusion_tag('accounts/includes/user_menu.html')
def user_menu(user):
    items = []

    # User is authenticated
    # header: caption of menu
    # items: menu items
    if user.is_authenticated():
        header = {'label': user.email}
        items.append({
            'label': 'Min side',
            'url': reverse('accounts_dashboard')
        })
        if user.is_staff:
            items.append({
                'label': 'Admin',
                'url': '/admin'
            })
        items.append({
            'label': 'Log ud',
            'url': reverse('auth_logout')
        })        
    
    # User is not authenticated
    # simply display login info
    else:
        header = {
            'label': 'Log ind',
            'url': reverse('auth_login')
        }
    return {'header': header, 'items': items}
