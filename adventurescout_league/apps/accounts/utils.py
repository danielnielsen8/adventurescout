from adventurescout_league.apps.league.models import Season
from adventurescout_league.apps.patrols.models import Patrol, SeasonalPatrol

class ExtUser(object):
    """
    An extended user class
    with helper funcs
    """
    def __init__(self, user):
        self.user = user

    def get_patrol(self):
        try:
            return self.patrol
        except AttributeError:
            self.patrol = Patrol.objects.get(owner=self.user)
            return self.patrol

    def get_seasonal_patrol(self):
        try:
            return self.seasonal_patrol
        except AttributeError:
            self.seasonal_patrol = SeasonalPatrol.objects.get(patrol=self.get_patrol(),
                                                              season=Season.get_current_season())
            return self.seasonal_patrol