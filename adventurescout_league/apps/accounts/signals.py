
def create_profile(sender, instance, request, **kwargs):
    from adventurescout_league.apps.accounts.models import LeagueProfile
    #If you want to set any values (perhaps passed via request) 
    #you can do that here

    LeagueProfile(user = instance).save()