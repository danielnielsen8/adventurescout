# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.accounts.models import LeagueProfile


class LeagueProfileAdmin(admin.ModelAdmin):
    search_fields = ['user__email', 'user__first_name', 'full_name']
    ordering = ['user__email']


admin.site.register(LeagueProfile, LeagueProfileAdmin)
