# -*- coding: utf-8
"""
The urls from django-registration and django.contrib.auth
is manually inserted here for the sake of better custimization
"""

from django.contrib.auth import views as auth_views
from django.conf.urls import url
from django.views.generic.base import TemplateView

from adventurescout_league.apps.accounts.views import (
    ActivationView, RegistrationView, DashboardView, ProfileCreate, ProfileEdit,
)
from adventurescout_league.apps.utils.email_forms import EmailAuthenticationForm


# Django contrib auth app url's
urlpatterns = [
    
    # Login / logout
    url(r'^login/$',
        auth_views.login,
        {'template_name': 'accounts/login.html', 'authentication_form': EmailAuthenticationForm},
        name='auth_login'),
    
    url(r'^logout/$',
        auth_views.logout,
        {'next_page': '/'},
        name='auth_logout'),
    
    # Password
    url(r'^password/change/$',
        auth_views.password_change,
        name='auth_password_change'),
    
    url(r'^password/change/done/$',
        auth_views.password_change_done,
        name='auth_password_change_done'),
    
    url(r'^password/reset/$',
        auth_views.password_reset,
        name='auth_password_reset'),
    
    url(r'^password/reset/confirm/(?P<uidb36>[0-9A-Za-z]+)-(?P<token>.+)/$',
        auth_views.password_reset_confirm,
        name='auth_password_reset_confirm'),
    
    url(r'^password/reset/complete/$',
        auth_views.password_reset_complete,
        name='auth_password_reset_complete'),
    
    url(r'^password/reset/done/$',
        auth_views.password_reset_done,
        name='auth_password_reset_done'),
]

# Accounts app urls 
urlpatterns += [

    # Signup
    url(r'^signup/$', RegistrationView.as_view(), name='accounts_signup'),
    url(r'^signup/complete/$', TemplateView.as_view(template_name='accounts/signup_complete.html'),
        name='accounts_signup_complete'),

    # Activation
    url(r'^activate/(?P<activation_token>\d+-\w+)/$', ActivationView.as_view(), name='accounts_activate'),
    url(r'^activate/complete/$', TemplateView.as_view(template_name='accounts/activation_complete.html'),
        name='accounts_activation_complete'),

    url(r'^dashboard/$', DashboardView.as_view(), name='accounts_dashboard'),
    url(r'^profile/create/$', ProfileCreate.as_view(), name='accounts_profile_create'),
    url(r'^profile/edit/(?P<pk>\d+)/$', ProfileEdit.as_view(), name='accounts_profile_edit'),
]
