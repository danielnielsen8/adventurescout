# -*- coding: utf-8

from __future__ import unicode_literals

from datetime import timedelta

from django.conf import settings

from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import get_user_model
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render
from django.template.loader import render_to_string, get_template
from django.views.generic import CreateView, UpdateView, View, TemplateView
from django.forms import ModelForm
from django.forms.models import modelform_factory
from django.contrib.auth.models import Group

from adventurescout_league.apps.accounts.models import LeagueProfile
from adventurescout_league.apps.patrols.models import Patrol
from adventurescout_league.apps.races.models import Race
from adventurescout_league.apps.tokens.models import Token
from adventurescout_league.apps.tokens.views import handle_token
from adventurescout_league.apps.utils.email_forms import EmailUserCreationForm
from adventurescout_league.apps.utils.widgets import DatePicker


def send_user_activation(user):
    ttl = timedelta(settings.ACCOUNT_ACTIVATION_DAYS, 0)
    user_token = Token.objects.create('account_token',
                                      {'user_id': user.pk}, ttl)
    user_token.save()

    # Send email
    ctx_dict = {'activation_token': user_token.full_code,
                'expiration_days': settings.ACCOUNT_ACTIVATION_DAYS,
                'site': settings.SITE_URL}
    subject = render_to_string(
        'accounts/activation_email_subject.txt', ctx_dict
    )
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())

    message = render_to_string('accounts/activation_email.txt', ctx_dict)

    user.email_user(subject, message, settings.DEFAULT_FROM_EMAIL)


def create_inactive_user(email, password):
    user = get_user_model().objects.create_user(username=email, email=email, password=password)
    user.is_active = False
    user.save()

    # TODO: SMTP needs to be configured
    # send_user_activation(user)

    return user


class LeagueProfileForm(ModelForm):
    class Meta:
        model = LeagueProfile
        exclude = ('user',)
        widgets = {
            'birth_date': DatePicker
        }


class ActivationView(TemplateView):
    """
    Activate the user
    """
    template_name = 'accounts/activate.html'

    def get(self, request, *args, **kwargs):
        from django.contrib.auth import get_user_model

        user_model = get_user_model()

        try:
            token = kwargs['activation_token']
            data = handle_token(request, token)
            user = user_model.objects.get(pk=data['user_id'])
            if not user.is_active:
                user.is_active = True
                user.save()
                return redirect(reverse('accounts_activation_complete'))
        except Exception:
            pass
        template = get_template(self.template_name)
        return template.render(request, context_dict={'activation_token': token})


class RegistrationView(View):
    """
    Custom registration view

    used when users are signing up from the 'frontpage'
    """
    user_form = EmailUserCreationForm
    profile_form = LeagueProfileForm
    template_name = 'accounts/signup_form.html'

    def get(self, request):
        context = {'user_form': self.user_form(), 'profile_form': self.profile_form()}
        return render(request, self.template_name, context=context)

    def post(self, request):
        user_form = self.user_form(data=request.POST, files=request.FILES)
        profile_form = self.profile_form(data=request.POST, files=request.FILES)

        # Setup user
        if user_form.is_valid():
            user_data = user_form.cleaned_data
            email, password = user_data['email'], user_data['password1']

            # Setup profile - only if user is valid
            if profile_form.is_valid():

                # Create inactive_user
                user = create_inactive_user(email, password)

                profile = profile_form.save(commit=False)
                profile.user = user
                profile.save()

                # Add groups
                self.add_default_groups(user)

                return redirect('accounts_signup_complete')

        context = {'user_form': user_form, 'profile_form': profile_form}
        return render(template_name=self.template_name, request=request, context=context)

    def add_default_groups(self, user):
        try:
            grp = Group.objects.get(name='Patrol admin')
            user.groups.add(grp)
        except Group.DoesNotExist:
            pass


class DashboardView(View, LoginRequiredMixin):
    """
    Dashboard showing relevant information for the user
    """
    template_name = 'accounts/dashboard.html'

    def get(self, request):
        user = request.user
        context = {'tabs': [], 'request': request}

        # Patrol
        try:
            patrol = Patrol.objects.get(owner=user)
        except Patrol.DoesNotExist:
            patrol = None

        context['tabs'].append({
            'id': 'tab-patrol',
            'caption': 'Patrulje',
            'perms': ('patrols.add_patrol',),
            'content_template': 'accounts/snippets/patrol_summary.html',
            'content_context': {'patrol': patrol}
        })

        # Registrations
        context['tabs'].append({
            'id': 'tab-registrations',
            'caption': 'Tilmeldinger',
            'perms': ('registrations.add_registration',),
            'content_template': 'accounts/snippets/registration_summary.html',
        })

        # Race
        try:
            race = Race.objects.get(owner=user)
        except Race.DoesNotExist:
            race = None
        except Race.MultipleObjectsReturned:
            race = None

        context['tabs'].append({
            'id': 'tab-race',
            'caption': 'Løb',
            'perms': ('races.add_race',),
            'content_template': 'accounts/snippets/race_summary.html',
            'content_context': {'race': race}
        })

        # Admissions
        context['tabs'].append({
            'id': 'tab-admissions',
            'caption': 'Sæsonoptagelser',
            'perms': ('registrations.add_seasonaladmission',),
            'content_template': 'accounts/snippets/admission_summary.html'
        })

        try:
            profile = LeagueProfile.objects.get(user=user)
        except Exception:
            profile = None

        context['profile'] = profile
        context['user'] = user
        context['request'] = request
        context['tabs'].append({
            'id': 'tab-profile',
            'caption': 'Profile',
            'content_template': 'accounts/snippets/profile_summary.html',
            'content_context': {'user': user, 'profile': profile, 'request': request}
        })
        return render(request=request, template_name=self.template_name, context=context)


class ProfileCreate(LoginRequiredMixin, CreateView):
    template_name = 'base/base_form.html'
    model = LeagueProfile

    def get_success_url(self):
        return reverse('accounts:accounts_dashboard')

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('user',))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save()

        return redirect(self.get_success_url())


class ProfileEdit(LoginRequiredMixin, UpdateView):
    """
    Profile edit view

    used on user pages - profile pane
    """
    template_name = 'base/base_form.html'
    model = LeagueProfile

    def get_success_url(self):
        return reverse('accounts_dashboard')

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('user',))
