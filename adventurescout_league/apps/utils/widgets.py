# -*- coding: utf-8
from django.forms import widgets


class TextPlaceholderInput(widgets.TextInput):
    def __init__(self, text=''):
        attrs = {'placeholder': text}
        super(TextPlaceholderInput, self).__init__(attrs=attrs)    


class DatePicker(widgets.DateTimeInput):
    def __init__(self):
        attrs = {'data-behavior': 'DatePicker',
                 'data-datepicker-format': '%d.%m.%Y',
                 'data-datepicker-start-view': 'years',
                 'placeholder': 'DD.MM.ÅÅÅÅ'}
        super(DatePicker, self).__init__(attrs=attrs)


class DateTimePicker(widgets.DateTimeInput):
    def __init__(self):
        attrs = {'data-behavior': 'DatePicker', 
                 'data-datepicker-time-picker': 'true',
                 'data-datepicker-format': '%d.%m.%Y %k:%M',
                 'placeholder': 'DD.MM.ÅÅÅÅ TT:MM'}
        super(DateTimePicker, self).__init__(attrs=attrs)