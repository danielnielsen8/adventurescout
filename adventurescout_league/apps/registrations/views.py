# -*- coding: utf-8
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.views.generic.base import View, TemplateView
from django.views.generic.edit import CreateView, UpdateView
from django.forms.models import modelform_factory

from adventurescout_league.apps.registrations.models import *
from adventurescout_league.apps.registrations.schemes import load_admission_scheme
from adventurescout_league.apps.utils.widgets import DateTimePicker


class AdmissionCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateView):
    model = SeasonalAdmission
    template_name = 'base/base_form.html'

    def get_form_class(self):
        return modelform_factory(model=SeasonalAdmission,
                                 widgets={'admission_start': DateTimePicker,
                                          'admission_end': DateTimePicker})

    def get_success_url(self):
        return reverse('accounts_dashboard')


class AdmissionUpdateView(LoginRequiredMixin, PermissionRequiredMixin, UpdateView):
    model = SeasonalAdmission
    template_name = 'base/base_form.html'

    def get_form_class(self):
        return modelform_factory(model=SeasonalAdmission,
                                 widgets={'admission_start': DateTimePicker,
                                          'admission_end': DateTimePicker})

    def get_success_url(self):
        return reverse('accounts_dashboard')


class AdmissionOverviewView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """
    Overview page for admission with list of all registrations
    """
    template_name = 'registrations/seasonaladmission_overview.html'
    required_perms = ('registrations.add_seasonaladmission',)

    def get_context_data(self, **kwargs):
        context = super(AdmissionOverviewView, self).get_context_data(**kwargs)
        try:
            seasonal_admission = SeasonalAdmission.objects.get(pk=kwargs['pk'])
        except SeasonalAdmission.DoesNotExist:
            raise Http404

        # Check ownership
        # if seasonal_admission.seasonal_race.race.owner.pk != self.request.user.pk:
        #     raise PermissionDenied

        context['seasonal_admission'] = seasonal_admission
        return context


class AdmissionExportView(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    """
    Export patrols from admisison as CSV
    """
    template_name = 'registrations/seasonaladmission_export.csv'
    required_perms = ('registrations.add_seasonaladmission',)

    def get_context_data(self, **kwargs):
        context = super(AdmissionExportView, self).get_context_data(**kwargs)
        try:
            seasonal_admission = SeasonalAdmission.objects.get(pk=kwargs['pk'])
        except SeasonalAdmission.DoesNotExist:
            raise Http404

        # Check ownership
        # if seasonal_admission.seasonal_race.race.owner.pk != self.request.user.pk:
        #     raise PermissionDenied

        context['seasonal_admission'] = seasonal_admission
        return context


class RegistrationView(View):

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.load_admission(kwargs['league_id'])
        self.load_scheme()
        return super(RegistrationView, self).dispatch(request, *args, **kwargs)

    def load_admission(self, league_id):
        # Get the (seasonal) admission
        # or raise Http404 if not found
        if not hasattr(self, 'admission'):
            try:
                self.admission = SeasonalAdmission.get_object(race__league_id=league_id)
            except SeasonalAdmission.DoesNotExist:
                raise Http404

    def load_scheme(self):
        if not hasattr(self, 'scheme'):
            self.scheme = load_admission_scheme(self.admission.scheme_name)

    def get(self, request, *args, **kwargs):
        return self.scheme.view(request, admission=self.admission)

    def post(self, request, *args, **kwargs):
        return self.scheme.view(request, admission=self.admission)
