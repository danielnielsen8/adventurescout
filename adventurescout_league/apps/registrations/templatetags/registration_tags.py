# -*- coding: utf-8

from __future__ import unicode_literals

from django import template
from adventurescout_league.apps.registrations.tables import *
from adventurescout_league.apps.registrations.models import *

register = template.Library()


@register.inclusion_tag('registrations/includes/race_seasonal_admission.html')
def race_seasonal_admission(race, user, request):
    """
    Show list of available admissions
    """
    admissions = SeasonalAdmission.objects.filter(seasonal_race__race=race)

    return {
        'admissions': admissions,
        'user': user,
        'request': request,
    }


@register.inclusion_tag('registrations/includes/user_seasonal_admissions.html')
def user_seasonal_admissions(user, request):
    """
    Show table of seasonal admissions
    """
    context = {'adm_table': SeasonalAdmissionsTable(SeasonalAdmission.objects.all()), 'request': request}
    return context


@register.inclusion_tag('registrations/includes/user_registrations.html')
def user_registrations(user, request):
    """
    Show table of registrations made by the user
    """
    registrations = Registration.objects.filter(seasonal_patrol__patrol__owner=user)
    context = {'registrations_table': RegistrationsTable(registrations), 'request': request}
    return context
