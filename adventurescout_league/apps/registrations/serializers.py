from django.forms import widgets

from adventurescout_league.apps.league.serializers import *
from adventurescout_league.apps.patrols.serializers import PatrolSerializer, PatrolMemberSerializer


class RegistrationSerializer(serializers.Serializer):
    league_id = serializers.CharField(
        source='seasonal_patrol.get_league_id', 
        read_only=True)
    created = serializers.DateField()

    season = SeasonSerializer(
        source='seasonal_patrol.season', 
        read_only=True)
    patrol = PatrolSerializer(
        source='seasonal_patrol.patrol', 
        read_only=True)
    competition_classes = CompetitionClassIdentifierSerializer(
        many=True, 
        source='seasonal_patrol.competition_classes',
        read_only=True)
    patrol_members = PatrolMemberSerializer(many=True)


class SeasonalAdmissionSerializer(serializers.Serializer):
    season = SeasonSerializer(
        source='seasonal_race.season', 
        read_only=True)
    is_open = serializers.BooleanField(source='is_open')
    status = serializers.CharField(source='get_status')
    link = serializers.CharField(source='get_absolute_url')
    competition_classes = CompetitionClassSerializer(
        many=True, 
        source='seasonal_race.competition_classes',
        read_only=True)