# -*- coding: utf-8
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django import forms
from django.utils.translation import ugettext as _
import django_tables2 as tables
from django.views.generic.base import View, TemplateResponseMixin

from adventurescout_league.apps.tokens.utils import create_serializer_token
from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.patrols.models import PatrolMember, SeasonalPatrol
from adventurescout_league.apps.registrations.models import Registration
from adventurescout_league.apps.registrations.serializers import RegistrationSerializer


class NathejkRegistrationView(TemplateResponseMixin, View):
    """
    View that handles the registration
    """
    template_name = 'registrations/schemes/registration_nathejk.html'

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.admission = kwargs['admission']
        self.season = self.admission.seasonal_race.season

        # Get seasonal patrol for user and the season which this admission belongs to if any
        try:
            self.seasonal_patrol = SeasonalPatrol.seasonals.get_by_user(self.request.user,
                                                                        self.admission.seasonal_race.season)
        except SeasonalPatrol.DoesNotExist:
            self.seasonal_patrol = False

        return super(NathejkRegistrationView, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        context = self.get_context()
        return self.render_to_response(context)

    def post(self, request, **kwargs):
        form = NathejkParticipantForm(request=request, data=request.POST)

        if self.seasonal_patrol and form.is_valid():
            registration = Registration(seasonal_patrol=self.seasonal_patrol,
                                        seasonal_admission=self.admission)
            registration.save()
            registration.patrol_members.add(*form.cleaned_data['include'])

            # Serialize registration and create token
            serializer = RegistrationSerializer(registration)
            token = create_serializer_token('registration_token', serializer)

            return HttpResponseRedirect('http://tilmelding.nathejk.dk/liga?token={}'
                                        .format(token.full_code))

        context = self.get_context()
        return self.render_to_response(context)

    def get_context(self):
        context = {}
        context['admission'] = self.admission
        context['season'] = self.season
        context['seasonal_patrol'] = self.seasonal_patrol

        if self.seasonal_patrol:
            context['member_table'] = NathejkParticipantTable(self.seasonal_patrol.members.all())

        return context

class NathejkParticipantForm(RequestFormMixin, forms.Form):
    include = forms.ModelMultipleChoiceField(queryset=PatrolMember.objects.all(),
                                             widget=forms.CheckboxSelectMultiple())


class NathejkRegistrationForm(RequestFormMixin, forms.Form):
    """
    Form for registration of additionally info
    """


class NathejkParticipantTable(tables.Table):
    """
    Table showing expanded view of patrols members
    """
    class Meta:
        model = PatrolMember
        sequence = ('include', 'name', '...')
        exclude = ('patrol','user','id')
        attrs = {'data-behavior': 'TableSelectAll'}

    include = tables.CheckBoxColumn(accessor='pk', verbose_name=_('participating?'))

# Define 'view'
view = NathejkRegistrationView.as_view()
