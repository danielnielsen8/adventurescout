# -*- coding: utf-8
import urllib
import json

from django.http import HttpResponseRedirect
from django.views.generic.base import View

class FenrisRegistrationView(View):
    def dispatch(self, request, *args, **kwargs):
        return HttpResponseRedirect('http://fenris.spejder.dk');

# Define 'view'
view = FenrisRegistrationView.as_view()
