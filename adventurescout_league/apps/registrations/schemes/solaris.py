# -*- coding: utf-8
import urllib
import json

from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django import forms
from django.utils.translation import ugettext as _
from django.views.generic.base import View, TemplateResponseMixin

import django_tables2 as tables

from adventurescout_league.apps.tokens.utils import create_serializer_token
from adventurescout_league.apps.accounts.models import LeagueUser
from adventurescout_league.apps.league.models import CompetitionClass
from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.patrols.models import PatrolMember, SeasonalPatrol
from adventurescout_league.apps.registrations.models import Registration
from adventurescout_league.apps.registrations.serializers import RegistrationSerializer

MAX_PARTICIPANTS = {
    'U': 200,
    'S': 200
}
MAX_MEMBER_COUNT = 6
MIN_MEMBER_COUNT = 3

class RegistrationError(Exception):
    pass


class SolarisRegistrationErrorView(TemplateResponseMixin, View):
    """
    Redirect to this view if any serious errors occur
    """
    template_name = 'registrations/registration_error.html'

    def dispatch(self, request, *args, **kwargs):
        self.request = request

        if 'error' in kwargs:
            error = kwargs['error']
        else:
            error = 'Ukendt fejl opstod! Vi beklager.'

        return self.render_to_response({'error': error})


class SolarisRegistrationView(TemplateResponseMixin, View):
    """
    View that handles the registration
    Special registraion scheme for Solaris, uses GruppeWeb for
    payments
    """
    template_name = 'registrations/schemes/registration_solaris.html'
    PRE_ASSIGNMENT_URL = 'http://tilmelding.danmarkieuropa.dk?ligaID='

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        # Secret registration test flag
        self.allow_test = False
        if 'reg_test' in request.GET:
            if request.GET['reg_test'] == '26':
                self.allow_test = True

        # Setup common variables
        self.league_user = LeagueUser.objects.get(pk=request.user.pk)
        self.request = request
        self.admission = kwargs['admission']
        self.season = self.admission.seasonal_race.season

        try:
            # These checks could raise an error
            self.seasonal_patrol = self.check_seasonal_patrol()
            self.check_admission()

            return super(SolarisRegistrationView, self).dispatch(request, *args, **kwargs)
        except RegistrationError as error:
            error_view = SolarisRegistrationErrorView()
            return error_view.dispatch(request, error=error, *args, **kwargs)

    def get(self, request, **kwargs):
        # Catch error if user has not created seasonal patrol
        if not self.seasonal_patrol:
            raise RegistrationError('Du skal oprette en sæsonpatrulje for {} \
                                     for at kunne tilmelde dig.'.format(
                                     self.season))

        # Make forms
        self.forms = {
            'registration': SolarisRegistrationForm(request=request),
            'participants': SolarisParticipantForm(request=request)
        }
        context = self.get_context()
        context['forms'] = self.forms

        return self.render_to_response(context)

    def post(self, request, **kwargs):

        # Check forms
        self.forms = {
            'registration': SolarisRegistrationForm(request=request, data=request.POST),
            'participants': SolarisParticipantForm(request=request, data=request.POST)
        }

        forms_valid = True
        for key, form in self.forms.items():
            forms_valid = forms_valid and form.is_valid()

        if forms_valid:
            return self.forms_valid()

        context = self.get_context()
        context['forms'] = self.forms

        return self.render_to_response(context)

    def forms_valid(self):
        # Make additionally checks
        participant_count = len(self.forms['participants'].cleaned_data['include'])
        # self.check_member_count(participant_count)
		# Not needed for 2014 race, there is a preassignment
        # self.check_max_participants(participant_count)
        ligaid=self.seasonal_patrol.get_league_id()
        # Make registration
        registration = Registration(seasonal_patrol=self.seasonal_patrol,
                                    seasonal_admission=self.admission,
                                    status=Registration.STATUS_PRE_ASSIGNMENT,
                                    url=self.PRE_ASSIGNMENT_URL)
        registration_data = json.dumps(self.forms['registration'].cleaned_data)
        registration.extra_data = registration_data

        registration.save()
        registration.patrol_members.add(*self.forms['participants'].cleaned_data['include'])

        # Redirect to GruppeWeb
        # query_str = self.get_gruppeweb_query(participant_count)
        #return HttpResponseRedirect('http://solaris.spejder.dk/node/266?{}'
        #                             .format(query_str))
		# Redirect to preassignment
        return HttpResponseRedirect(self.PRE_ASSIGNMENT_URL + ligaid)

    def get_context(self):
        context = {}
        context['admission'] = self.admission
        context['season'] = self.season
        context['seasonal_patrol'] = self.seasonal_patrol

        # We should be pretty sure that self.seasonal_patrol is not none!
        context['patrol_members'] = self.seasonal_patrol.members.all()

        context['min_member_count'] = MIN_MEMBER_COUNT
        context['max_member_count'] = MAX_MEMBER_COUNT

        # Get current participant count
        u_count = Registration.objects.participant_count(self.admission,
                  CompetitionClass.objects.get(identifier='U'))
        s_count = Registration.objects.participant_count(self.admission,
                  CompetitionClass.objects.get(identifier='S'))

        context['participants_open_slots'] = {
            'U': '{} (ud af {})'.format(MAX_PARTICIPANTS['U'] - u_count, MAX_PARTICIPANTS['U']),
            'S': '{} (ud af {})'.format(MAX_PARTICIPANTS['S'] - s_count, MAX_PARTICIPANTS['S'])
        }
        return context

    def get_gruppeweb_query(self, member_count):
        """
        Get query data for GruppeWeb
        """
        assert type(member_count) is int

        member_count_map = {
            3: 160,
            4: 161,
            5: 162,
            6: 163
        }
        member_count = member_count_map[member_count]
        query_str = 'edit[attributes][131]={ligaid}&edit[attributes][47]={member_count}'.format(
                     ligaid=self.seasonal_patrol.get_league_id(),
                     member_count=member_count)
        return query_str

    def check_admission(self):
        """
        Check admission

        is it open?
        Does it allow competition class of the seasonal patrol?
        """

        # Admission is open
        if not self.admission.is_open() and not self.allow_test:
            raise RegistrationError('<h3 class="red-clr">Tilmelding er ikke åben!</h3>\
                                     <p>Læs nærmere på løbets side.</p>')

        # Comp class for patrol is allowed
        comp_classes = [c for c in self.seasonal_patrol.get_comp_classes() if c.category == 'age']
        if len(comp_classes) == 1:
            age_class = comp_classes[0]
            if age_class not in self.admission.seasonal_race.competition_classes.all():
                raise RegistrationError('<h3 class="red-clr">Klasse ikke tilladt!</h3> \
                                         <p>Det er ikke muligt at tilmelde sig <em>{seasonal_race}</em> med \
                                         klassen: <em>{comp_class}</em>'.format(
                                         seasonal_race=self.admission.seasonal_race,
                                         comp_class=age_class))
        else:
            raise RegistrationError('Seasonal patrol did not have one and only one age class!')


    def check_seasonal_patrol(self):
        """
        Check for seasonal patrol
        """
        sp = self.league_user.patrol.get_seasonal(self.season)

        # Seasonal patrol is not created
        if not sp:
            raise RegistrationError('Du skal oprette en sæsonpatrulje for {} \
                                     for at kunne tilmelde dig.'.format(
                                     self.season))

        # Seasonal patrol is not already registred for this admission
        # TODO: should this be 'unique together'
        c = Registration.objects.filter(seasonal_patrol=sp,
                                        seasonal_admission=self.admission).count()
        if c > 0 and not self.allow_test:
            raise RegistrationError('Du har allerede tilmeldt din sæsonpatrulje til <em>{}</em> -<br>Se tilmeldinger under "Min side" <a href="/accounts/dashboard/">her</a>'.format(
                                     self.admission))
        return sp

    def check_max_participants(self, participant_count):
        """
        Check if max participants has been
        reached for given age class

        *participant_count* is the number of desired patrolmembers to register

        Returns remaining open 'slots' for participants
        """
        assert type(participant_count) is int

        comp_classes = [c for c in self.seasonal_patrol.get_comp_classes() if c.category == 'age']
        if len(comp_classes) == 1:
            age_class = comp_classes[0]
            overall_count = Registration.objects.participant_count(self.admission, comp_classes[0])
            if (overall_count + participant_count) <= MAX_PARTICIPANTS[age_class.identifier]:
                return overall_count + participant_count
            else:
                raise RegistrationError('<h3 class="red-clr">Deltagerantal for klassen: <em>{cls}</em> er overtegnet!</h3> \
                                         <p>Der er {overall_count} tilmeldte og du forsøgte at tilmelde {participant_count}.<br /> \
                                         Det totale antal tilladte deltager for denne klasse er: {max_participant}</p>'.format(
                                         cls=age_class, overall_count=overall_count,
                                         participant_count=participant_count,
                                         max_participant=MAX_PARTICIPANTS[age_class.identifier]))
        else:
            raise RegistrationError('Seasonal patrol did not have one and only one age class!')


def validate_participant_count(value):
    if not MAX_MEMBER_COUNT >= len(value) >= MIN_MEMBER_COUNT:
        raise ValidationError(u'Ønsket deltagerantal skal være imellem {} og {}.'.format(
                              MIN_MEMBER_COUNT, MAX_MEMBER_COUNT))


class SolarisParticipantForm(RequestFormMixin, forms.Form):
    include = forms.ModelMultipleChoiceField(queryset=PatrolMember.objects.all(),
                                             widget=forms.CheckboxSelectMultiple(),
                                             validators=[validate_participant_count])


class SolarisRegistrationForm(RequestFormMixin, forms.Form):
    """
    Form for registration of additionally info
    """
    allergy = forms.CharField(max_length=1024, required=False,
                              widget=forms.Textarea(attrs={'rows': 6}))
    #disallow_photo = forms.BooleanField(required=False)

    allergy.label = 'Allergier / Bemærkninger'
    #disallow_photo.label = 'Tillad ikke billeder'

    allergy.help_text = 'Hvis der er nogen i jeres patrulje der har allergier eller andre bemærkninger, notér det venligst her'
    #disallow_photo.help_text = 'Afkryds her hvis en person fra jeres patrulje *ikke* ønsker at blive fotograferet'


# Define 'view'
view = SolarisRegistrationView.as_view()
