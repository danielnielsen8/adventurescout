# -*- coding: utf-8
"""
Base registrations scheme which others schemes can inherit from
"""
import json

from django import forms
from django.views.generic.base import View, TemplateResponseMixin

from adventurescout_league.apps.accounts.models import LeagueUser
from adventurescout_league.apps.league.models import CompetitionClass
from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.patrols.models import PatrolMember
from adventurescout_league.apps.registrations.models import Registration


class RegistrationError(Exception):
    pass


class BaseRegistrationErrorView(TemplateResponseMixin, View):
    """
    Redirect to this view if any serious errors occur
    """
    template_name = 'registrations/registration_error.html'

    def dispatch(self, request, *args, **kwargs):
        self.request = request

        if 'error' in kwargs:
            error = kwargs['error']
        else:
            error = 'Ukendt fejl opstod! Vi beklager.'
        return self.render_to_response({'error': error})


class BaseRegistrationView(TemplateResponseMixin, View):
    """
    View that handles the registration
    """
    template_name = 'registrations/schemes/registration_invictus.html'
    # Type this in url reg_test=narnia to set allow_test flag to true
    secret_test_pass = 'narnia'
    max_participants = {
        'U': 100, 
        'S': 100
    }
    max_member_count = 6
    min_member_count = 3

    def dispatch(self, request, *args, **kwargs):
        # Secret registration test flag
        self.allow_test = False
        if 'reg_test' in request.GET:
            if request.GET['reg_test'] == self.secret_test_pass:
                self.allow_test = True

        # Setup common variables
        self.league_user = LeagueUser.objects.get(pk=self.request.user.pk)
        self.request = request
        self.admission = kwargs['admission']
        self.season = self.admission.seasonal_race.season
        
        try:
            # Fetch users seasonal patrol
            try:
                self.seasonal_patrol = self.league_user.patrol.get_seasonal(self.season)    
            except AttributeError:
                raise RegistrationError('Du skal oprette en patrulje for at kunne tilmelde dig.')

            # Perform prechecks
            self.pre_checks()
        
            # continue with processing and
            # return error view something goes wrong
            return super(BaseRegistrationView, self).dispatch(request, *args, **kwargs)
        except RegistrationError as error:
            error_view = BaseRegistrationErrorView()
            return error_view.dispatch(request, error=error, *args, **kwargs)


    def get(self, request, **kwargs):
        """
        Registration consists of two forms

        * registration form: Specific data for this registration
        * participants form: Registration of participants
        """
        context = self.get_context()
        context['forms'] = self.get_forms()

        return self.render_to_response(context)

    def post(self, request, **kwargs):
        self.forms = self.get_forms(request.POST)
        
        # Validate forms
        forms_valid = True
        for key, form in self.forms.items():
            forms_valid = forms_valid and form.is_valid()

        # Validation successful
        if forms_valid:
            self.post_checks()
            return self.forms_valid()
        
        # Validation not successfull
        # show errors
        context = self.get_context()
        context['forms'] = self.forms

        return self.render_to_response(context)

    def forms_valid(self):
        """
        Take action if forms are valid
        *Remmber this is not a isValid check but a success callback
        """
        # Make registration
        registration = Registration(seasonal_patrol=self.seasonal_patrol, 
                                    seasonal_admission=self.admission)
        registration_data = json.dumps(self.forms['registration'].cleaned_data)
        registration.extra_data = registration_data

        registration.save()
        registration.patrol_members.add(*self.forms['participants'].cleaned_data['include'])

    def get_context(self):
        context = {}
        context['admission'] = self.admission        
        context['season'] = self.season
        context['seasonal_patrol'] = self.seasonal_patrol
        
        # We should be pretty sure that self.seasonal_patrol is not none!
        context['patrol_members'] = self.seasonal_patrol.members.all()

        context['min_member_count'] = self.min_member_count
        context['max_member_count'] = self.max_member_count

        # Get current participant count
        u_count = Registration.objects.participant_count(
            self.admission, CompetitionClass.objects.get(identifier='U')
        )
        s_count = Registration.objects.participant_count(
            self.admission, CompetitionClass.objects.get(identifier='S')
        )

        context['participants_open_slots'] = {
            'U': '{} (ud af {})'.format(self.max_participants['U'] - u_count, self.max_participants['U']),
            'S': '{} (ud af {})'.format(self.max_participants['S'] - s_count, self.max_participants['S'])
        }
        return context

    def get_forms(self, data = None):
        """
        ** Get forms ***
        """
        RegistrationForm = getattr(self, 'registration_form_cls', BaseRegistrationForm)
        ParticipantsForm = getattr(self, 'participants_form_cls', BaseParticipantsForm)

        forms = {}
        formKwargs = {}

        # Figure out form arguments
        formKwargs['request'] = self.request
        if (data):
            formKwargs['data'] = self.request.POST
        
        # Construct forms        
        forms['registration'] = RegistrationForm(**formKwargs)
        forms['participants'] = ParticipantsForm(**formKwargs)

        return forms

    def pre_checks(self):
        """
        Checks to be run before forms is shown (GET)
        or form is processed (POST) 

        This is usually things like:
        * Does the user have a correct seasonal patrol
        * Is Admission open
        etc.
        """        
        self.check_seasonal_patrol()
        self.check_admission()
            
    def post_checks(self):
        """
        Checks to be run just before registration
        is actually created

        This is usually things like:
        * Are there still any open slots
        etc.
        """
        participant_count = len(self.forms['participants'].cleaned_data['include'])
        self.check_participants_count(participant_count)
        self.check_max_participants(participant_count)

    def check_admission(self):
        """
        ** Check admission **

        * is it open? (ignored if 'allow_test')
        * Does it allow competition class of users seasonal patrol?

        Raise RegistrationError if not.
        """

        # Admission is open
        if not self.admission.is_open() and not self.allow_test:
            raise RegistrationError('<h3 class="red-clr">Tilmelding er ikke åben!</h3>\
                                     <p>Læs nærmere på løbets side.</p>')

        # Comp class for patrol is allowed
        comp_classes = [c for c in self.seasonal_patrol.get_comp_classes() if c.category == 'age']
        if len(comp_classes) == 1:
            age_class = comp_classes[0]
            if age_class not in self.admission.seasonal_race.competition_classes.all():
                raise RegistrationError('<h3 class="red-clr">Klasse ikke tilladt!</h3> \
                                         <p>Det er ikke muligt at tilmelde sig <em>{seasonal_race}</em> med \
                                         klassen: <em>{comp_class}</em>'.format(
                                         seasonal_race=self.admission.seasonal_race, 
                                         comp_class=age_class))
        else:
            raise RegistrationError('Sæsonpatruljen havde ikke en (og kun en!) aldersklasse!')

    def check_seasonal_patrol(self):
        """
        ** Check for seasonal patrol **
        """
        sp = self.league_user.patrol.get_seasonal(self.season)

        # Seasonal patrol is not created
        if not sp:
            raise RegistrationError('Du skal oprette en sæsonpatrulje for {} \
                                     for at kunne tilmelde dig.'.format(
                                     self.season))
        
        # Seasonal patrol is not already registred for this admission
        # TODO: should this be 'unique together'
        c = Registration.objects.filter(seasonal_patrol=sp,
                                        seasonal_admission=self.admission).count()
        if c > 0 and not self.allow_test:
            raise RegistrationError('Du har allerede tilmeldt din sæsonpatrulje til <em>{}</em>'.format(
                                     self.admission))

    def check_participants_count(self, participant_count):
        if not self.max_member_count >= participant_count >= self.min_member_count:
            raise RegistrationError(u'Ønsket deltagerantal skal være imellem {} og {}.'.format(
                                    self.min_member_count, self.max_member_count))

    def check_max_participants(self, participant_count):
        """
        ** Check if max participants has been 
        reached for given age class **

        *participant_count* is the number of desired patrolmembers to register

        Returns remaining open 'slots' for participants
        """
        assert type(participant_count) is int

        comp_classes = [c for c in self.seasonal_patrol.get_comp_classes() if c.category == 'age']
        if len(comp_classes) == 1:
            age_class = comp_classes[0]
            overall_count = Registration.objects.participant_count(self.admission, comp_classes[0])
            if (overall_count + participant_count) <= self.max_participants[age_class.identifier]:
                return overall_count + participant_count
            else:
                raise RegistrationError('<h3 class="red-clr">Deltagerantal for klassen: <em>{cls}</em> er overtegnet!</h3> \
                                         <p>Der er {overall_count} tilmeldte og du forsøgte at tilmelde {participant_count}.<br /> \
                                         Det totale antal tilladte deltager for denne klasse er: {max_participant}</p>'.format(
                                         cls=age_class, overall_count=overall_count, 
                                         participant_count=participant_count, 
                                         max_participant=self.max_participants[age_class.identifier]))
        else:
            raise RegistrationError('Sæsonpatruljen havde ikke en (og kun en!) aldersklasse!')


class BaseParticipantsForm(RequestFormMixin, forms.Form):
    include = forms.ModelMultipleChoiceField(queryset=PatrolMember.objects.all(),
                                             widget=forms.CheckboxSelectMultiple())


class BaseRegistrationForm(RequestFormMixin, forms.Form):
    """
    Form for registration of additionally info
    """
    pass



