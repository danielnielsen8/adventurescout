# -*- coding: utf-8
"""
Base registrations scheme which others schemes can inherit from
"""
import json
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.utils.safestring import mark_safe
from django import forms
from django.views.generic.base import TemplateResponseMixin, View
from django.template.loader import render_to_string
from django.template import Context

from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.registrations.models import Registration
from .base import BaseRegistrationView, RegistrationError


class InvictusRegistrationForm(RequestFormMixin, forms.Form):
    """
    Form for registration of additionally info
    """
    primary_phone = forms.CharField(max_length=12)
    secondary_phone = forms.CharField(max_length=12)
    comments = forms.CharField(max_length=200)
    accepted = forms.BooleanField(required=True)

    primary_phone.label = '1. telefon'
    primary_phone.help_text = 'skal medbringes under løbet'

    secondary_phone.label = '2. telefon'
    secondary_phone.help_text = 'skal medbringes under løbet'

    comments.label = 'Kommentarer (medicin, allergier, mv.)'
    comments.help_text = 'Angiv detaljer for hele patruljen'

    accepted.label = 'Jeg accepterer invictus\' regler'
    accepted.help_text = mark_safe('Har du læst of forstået vore regler? <a target="_BLANK" href="http://invictusløbet.dk/regler">invictusløbet.dk/regler</a>')


# class InvictusParticipantsForm(RequestFormMixin, forms.Form):
#     include = forms.ModelMultipleChoiceField(queryset=PatrolMember.objects.all(),
#                                              widget=forms.CheckboxSelectMultiple())

UNIT_PRICE = 250
TSHIRT_PRICE = 100
STAMP_STD_PRICE = 20


class InvictusCompleteView(TemplateResponseMixin, View):
    template_name = 'registrations/schemes/complete_invictus.html'

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        return self.render_to_response({
            'registration_data': kwargs['registration_data'],
            'registration': kwargs['registration']
            })


class InvictusRegistrationView(BaseRegistrationView):
    registration_form_cls = InvictusRegistrationForm
    # participants_form_cls = InvictusParticipantsForm

    max_participants = {
        'U': 90,
        'S': 0
    }
    max_member_count = 6
    min_member_count = 3

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(InvictusRegistrationView, self).dispatch(request, *args, **kwargs)

    def forms_valid(self):
        """
        Take action if forms are valid
        *Remmber this is not a isValid check but a success callback
        """
        # Make registration
        registration = Registration(seasonal_patrol=self.seasonal_patrol,
                                    seasonal_admission=self.admission)

        # Price
        price_total = 0

        # Fetch out hardcodded merchandise form data
        merch_data = []
        stamps_std = self.request.POST.getlist('stamp_std')
        tshirts = self.request.POST.getlist('tshirt')

        for i, val in enumerate(self.forms['participants'].cleaned_data['include']):
            stamp_std = 1 if unicode(val.pk) in stamps_std else 0
            tshirt = tshirts[i]

            # Calculate price
            price = UNIT_PRICE
            price += TSHIRT_PRICE if (tshirt != '0') else 0
            price += STAMP_STD_PRICE if stamp_std else 0
            price_total += price

            merch_data.append({
                'pk': val.pk,
                'name': val.name,
                'tshirt': tshirt,
                'stamp_std': stamp_std,
                'price': price
                })

        # Compare price to price calculated in frontend
        if price_total != int(self.request.POST['total_price']):
            raise RegistrationError('Der opstod en fejl i sammenregning af pris')

        registration_data = self.forms['registration'].cleaned_data
        registration_data['merchandise'] = merch_data
        registration_data['price'] = price_total
        registration.extra_data = json.dumps(registration_data)

        registration.save()
        registration.patrol_members.add(*self.forms['participants'].cleaned_data['include'])

        # Send mail
        msg = render_to_string('registrations/schemes/invictus_confirmation_mail.txt', Context({
            'registration_data': registration_data,
            'patrol_members': registration.patrol_members
            }))
        send_mail('Tilmelding til Invictusloebet 2017', msg, 'info@adventurespejd.dk', [self.request.user.email])

        complete_view = InvictusCompleteView()
        return complete_view.dispatch(request=self.request, registration_data=registration_data,
                                      registration=registration)


view = InvictusRegistrationView.as_view()
