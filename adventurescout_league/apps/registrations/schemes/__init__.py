from importlib import import_module


class AdmissionSchemeError(Exception):
    pass


def load_admission_scheme(name):
    try:
        scheme_module = import_module('adventurescout_league.apps.registrations.schemes.' + name)
    except ImportError as error:
        raise AdmissionSchemeError(
            'Admission scheme with the name: {} could not be loaded, or is invalid.'.format(name)
        )
    return scheme_module
