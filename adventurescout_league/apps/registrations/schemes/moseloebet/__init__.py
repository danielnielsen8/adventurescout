# -*- coding: utf-8
from django import forms
from django.utils.translation import ugettext as _
import django_tables2 as tables

from adventurescout_league.apps.accounts.utils import ExtUser
from adventurescout_league.apps.league.tables import ControlColumn
from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.patrols.models import PatrolMember

class StandardRegistrationForm(RequestFormMixin, forms.Form):
    """
    Table showing expanded view of patrols members
    """
    LOK_CHOICES = (
        ('lok_1','LOK 1',),
        ('lok_2','LOK 2',),
        ('lok_3','LOK 3',),
    )
    primary_contact = forms.ModelChoiceField(queryset=PatrolMember.objects.all())
    lok = forms.ChoiceField(choices=LOK_CHOICES)

    lok.label = 'Lokal Område Kontrol'
    lok.help_text = 'I kan læse mere om de enkelte LOK\'s arbejdsopgaver under Moseløbet i jeres deltagerbrev'
    primary_contact.label = 'Primære kontaktperson'
    primary_contact.help_text = 'Vælg venligst den primære kontaktperson i jeres patrulje. Det er vigtigt at denne person kan kontaktes af teamet under hele Nathejk. Dvs. denne person skal medbringe en opladt mobiltelefon'

    def __init__(self, *args, **kwargs):
        super(StandardRegistrationForm, self).__init__(*args, **kwargs)
        if 'request' in kwargs:
            ext_user = ExtUser(kwargs['request'].user)
            self.fields['primary_contact'].queryset = ext_user.get_seasonal_patrol().members.all()

class StandardParticipantTable(tables.Table):
    """
    Table showing expanded view of patrols members
    """
    class Meta:
        model = PatrolMember
        sequence = ('include', 'name', '...')
        exclude = ('patrol','user','id')

    include = tables.CheckBoxColumn(accessor='pk', verbose_name=_('participating?'))
    participated_before = ControlColumn(name='participated_before', accessor='pk', 
                                        verbose_name=_('participated before?'))    

# Definitions to the admission

form = StandardRegistrationForm,
participant_table = StandardParticipantTable