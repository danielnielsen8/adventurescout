# -*- coding: utf-8
from django.conf.urls import url
from adventurescout_league.apps.registrations.views import *

urlpatterns = [
    # Seasonal Patrols
    url(r'^admission/create/$', AdmissionCreateView.as_view(), name='admission_create'),
    url(r'^admission/edit/(?P<pk>\d+)/$', AdmissionUpdateView.as_view(), name='admission_edit'),
    url(r'^admission/overview/(?P<pk>\d+)/$', AdmissionOverviewView.as_view(), name='admission_overview'),
    url(r'^admission/export/(?P<pk>\d+)/export.csv$', AdmissionExportView.as_view(), name='admission_export'),

    url(r'^(?P<league_id>R\d{2}\w{2}\d{3})/$', RegistrationView.as_view(), name='registration_registration')
]
