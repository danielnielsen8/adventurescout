# -*- coding: utf-8

from __future__ import unicode_literals

from django.contrib import admin
from adventurescout_league.apps.registrations.models import SeasonalAdmission, Registration


class SeasonalAdmissionAdmin(admin.ModelAdmin):
    pass


class RegistrationAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'seasonal_admission', 'seasonal_patrol', 'participants_count',)
    list_filter = ('seasonal_admission',)
    filter_vertical = ('patrol_members',)


admin.site.register(SeasonalAdmission, SeasonalAdmissionAdmin)
admin.site.register(Registration, RegistrationAdmin)
