# -*- coding: utf-8
from django.utils.safestring import SafeText
import django_tables2 as tables
from django_tables2.utils import A

from adventurescout_league.apps.registrations.models import *


class SeasonalAdmissionsTable(tables.Table):
    """
    Table showing Seasonal Admissions
    """
    class Meta:
        model = SeasonalAdmission
        exclude = ()

    actions = tables.Column(orderable=False, accessor='pk')
    name = tables.LinkColumn('admission_overview', args=[A('pk')])

    def render_actions(self, value):
        return SafeText(u'<a href="{edit_link}">rediger</a> | <a href="{overview_link}">oversigt</a> | <a href="{export_link}">exportér</a>'.format(
            edit_link=reverse('admission_edit', kwargs={'pk':value}),
            overview_link=reverse('admission_overview', kwargs={'pk':value}),
            export_link=reverse('admission_export', kwargs={'pk':value})
            ))


class RegistrationsTable(tables.Table):
    """
    Table showing Registrations
    """
    class Meta:
        model = Registration
        exclude = ('id', 'extra_data', 'seasonal_admission',)
        sequence = ('season', 'seasonal_patrol', 'seasonal_race', 'status', 'url', 'created',)
        orderable = False

    season = tables.Column(accessor='seasonal_patrol.season')
    seasonal_race = tables.Column(verbose_name='Sæsonløb', accessor='seasonal_admission.seasonal_race')

    def render_actions(self, value):
        return SafeText(u'Læs mere <i></i>')
