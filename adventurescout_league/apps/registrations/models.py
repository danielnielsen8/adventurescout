# -*- coding: utf-8

from __future__ import unicode_literals

import json

from django.core.urlresolvers import reverse
from django.http import Http404
from django.utils import formats, timezone
from django.db import models
from django.utils.encoding import python_2_unicode_compatible

from adventurescout_league.apps.tokens import register
from adventurescout_league.apps.races.models import SeasonalRace
from adventurescout_league.apps.patrols.models import PatrolMember, SeasonalPatrol


# Register tokens
@register('registration_token')
def registration_token_handler(request, token):
    # If token has expired raise '404 Not Found'
    if token.has_expired:
        raise Http404
    return token.data


class RegistrationManager(models.Manager):
    def participant_count(self, seasonal_admission, comp_class):
        """
        Count participants registrered to *seasonal_admission*
        belonging to seasonal patrols with *comp_class*
        """
        return PatrolMember.objects.filter(
            registration__seasonal_admission=seasonal_admission,
            registration__seasonal_patrol__competition_classes__pk=comp_class.pk,
        ).count()


admission_scheme_choices = (
    ('default', 'Default',),
    ('nathejk', 'Nathejk',),
    ('solaris', 'Solaris',),
    ('invictus', 'Invictus',),
    ('fenris', 'Fenris',),
)


@python_2_unicode_compatible
class SeasonalAdmission(models.Model):
    """
    Seasonal Admission
    """
    name = models.CharField(max_length=150)
    admission_start = models.DateTimeField()
    admission_end = models.DateTimeField()
    seasonal_race = models.OneToOneField(SeasonalRace, unique=True, on_delete=models.CASCADE)
    scheme_name = models.CharField(max_length=50, choices=admission_scheme_choices)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        """
        Get link to registration
        """
        return reverse('registration_registration',
                       kwargs={'league_id': self.seasonal_race.get_league_id()})

    def is_open(self):
        """
        Is seasonal admission currently open?
        returns Boolean
        """
        return self.admission_start < timezone.now() < self.admission_end

    def get_status(self):
        """
        Textual representation of the admissions status
        """
        format_localtime = lambda d: formats.date_format(timezone.localtime(d), format='DATETIME_FORMAT')
        status = ''

        if self.is_open():
            status = u'Tilmelding er åben. Den lukker igen {}.'.format(format_localtime(self.admission_end))
        elif self.admission_start > timezone.now():
            status = u'Tilmeldingen er ikke åben. Den åbner {}.'.format(format_localtime(self.admission_start))
        elif self.admission_end < timezone.now():
            status = u'Tilmeldingen for denne sæson er afsluttet!'

        return status

    @classmethod
    def get_object(cls, race__league_id=''):
        sr = SeasonalRace.get_object(league_id=race__league_id)
        return cls.objects.get(seasonal_race=sr)


@python_2_unicode_compatible
class Registration(models.Model):
    """
    A registration to a race
    """
    STATUS_NONE = 'None'
    STATUS_SIGNED_UP = 'SignedUp'
    STATUS_PRE_ASSIGNMENT = 'PreAssignment'
    STATUS_AWAITING_PAYMENT = 'AwaitingPayment'
    STATUS_NOT_SIGNED_UP = 'NotSignedUp'
    STATUS_WAITING_LIST = 'WaitingList'

    STATUS_CHOICES = (
        (STATUS_NONE, 'Ingen'),
        (STATUS_SIGNED_UP, 'Tilmeldt'),
        (STATUS_PRE_ASSIGNMENT, 'Udtagelsesprocess'),
        (STATUS_AWAITING_PAYMENT, 'Afventer betaling'),
        (STATUS_NOT_SIGNED_UP, 'Ej udtaget'),
        (STATUS_WAITING_LIST, 'Venteliste'),
    )

    seasonal_patrol = models.ForeignKey(SeasonalPatrol, on_delete=models.CASCADE)
    seasonal_admission = models.ForeignKey(SeasonalAdmission, on_delete=models.CASCADE)
    patrol_members = models.ManyToManyField(PatrolMember)
    extra_data = models.TextField(blank=True)
    created = models.DateTimeField(auto_now_add=True)
    status = models.CharField(max_length=20, choices=STATUS_CHOICES, default=STATUS_NONE)
    url = models.CharField(max_length=255, blank=True)

    # Custom manager
    objects = RegistrationManager()

    # Verbose names
    seasonal_patrol.verbose_name = SeasonalPatrol._meta.verbose_name
    created.verbose_name = 'Oprettet'

    def __str__(self):
        return '{} ({}) -> {}'.format(
            self.seasonal_admission,
            self.seasonal_admission.seasonal_race,
            self.seasonal_patrol,
        )

    def participants_count(self):
        return self.patrol_members.all().count()

    def get_extra_data(self):
        try:
            return json.loads(self.extra_data)
        except ValueError:
            return {}
