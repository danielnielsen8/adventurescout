"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from django.test import TestCase

from adventurescout_league.apps.accounts.models import LeagueProfile
from adventurescout_league.apps.utils.email_utils import create_user


class RegistrationBaseSchemeTest(TestCase):
    fixtures = ['../../var/test_fixtures.json']

    def setUp(self):
        """
        Setup dummy seasonal patrol
        """
        user = create_user('johndoe@example.com', '1234')
        profile = LeagueProfile.objects.create(
            full_name='John Doe',
            sex='M',
            address='Nowhere',
            postal_code='1234',
            city='Nowhere',
            phone='99999999',
            birth_date='1990-01-01',
            allow_notifications=True,
            user=user
        )

        profile.save()

    def test_registration(self):
        self.assertIs(self.client.login(username='johndoe@example.com', password='1234'), True)



