import django_tables2 as tables
from django.core.urlresolvers import reverse
from django.utils.safestring import SafeText
from django.utils.translation import ugettext as _

from adventurescout_league.apps.patrols.models import PatrolMember, SeasonalPatrol


class PatrolTable(tables.Table):
    class Meta:
        model = SeasonalPatrol
        fields = ('league_id', 'patrol', 'class_age', 'class_competition',)
        order_by = ('patrol',)

    league_id = tables.Column(accessor='get_league_id', order_by=('serial_num'), 
                              verbose_name='LigaID', attrs={"td": {"class": "monospaced"}})
    patrol = tables.LinkColumn('patrol_detail', args=[tables.utils.A('patrol.pk')])

    class_age = tables.Column(accessor='get_competition_classes', 
        verbose_name=_('Age class'), orderable=False)
    class_competition = tables.Column(accessor='get_competition_classes', verbose_name=_('Competition class'), orderable=False)

    def render_class_age(self, value):
        """
        Return age classes as nice string
        """
        s = ', '.join([c.name for c in value if c.category == 'age'])
        return s if s else _('None')
            
    def render_class_competition(self, value):
        """
        Return competition classes as nice string
        """        
        s = ', '.join([c.name for c in value if c.category == 'competition'])
        return s if s else _('None')   


class PatrolMembersTable(tables.Table):
    """
    Table showing Patrols members 
    """    
    class Meta:
        model = PatrolMember
        fields = ('name', 'birth_date')


class UserSeasonalPatrolsTable(tables.Table):
    """
    Table showing Seasonal patrols belonging to an user 
    """
    season = tables.Column(verbose_name=_('Season'))
    seasonal_patrol = tables.Column(verbose_name=_('Season patrulje'))
    status = tables.Column(verbose_name=_('Status'))
    actions = tables.Column(
        orderable=False, verbose_name=_('Actions'),
        attrs={
            "td": {"class": "align-right"},
            "th": {"class": "align-right"}
        }
    )

    def render_actions(self, value):
        if value:
            if value['op'] == 'create':
                label = _('create')
                link = reverse('seasonal_patrol_create', kwargs={'season_num': value['season_num']})
                html_class = ('button-small', 'red')
            elif value['op'] == 'edit':
                label = _('edit')
                link = reverse('seasonal_patrol_edit', kwargs={'pk': value['pk']})
                html_class = ('button-small',)

            return SafeText('<a class="{}" href="{}">{}</a>'.format(' '.join(html_class),
                                                             link, label))    
        return '-'


class UserPatrolMembersTable(tables.Table):
    """
    Table showing Patrols members belonging to an user 
    """
    class Meta:
        model = PatrolMember
        exclude = ('patrol', 'user', 'id')

    def render_actions(self, value):
        return SafeText('<a href="{}">{}</a>'.format(reverse('patrol_member_edit', kwargs={'pk':value}), _('edit')))
