# -*- coding: utf-8
import django.forms as forms
from adventurescout_league.apps.league.models import CompetitionClass
from adventurescout_league.apps.league.forms import RequestFormMixin
from adventurescout_league.apps.patrols.models import SeasonalPatrol


class SeasonalPatrolForm(RequestFormMixin, forms.ModelForm):
    """
    Custimized form for creating/updating seasonal patrols.

    - m2m field: 'competition_classes' is translated into to individual 
      model choice fields: 'age_class' and 'comp_class'
      on save and instantiation
    - foreignKey related field: 'members' is changed to checkbox widget
    """
    class Meta:
        model = SeasonalPatrol
        exclude = ('members', 'patrol', 'season', 'competition_classes')
        widgets = {
            'members': forms.CheckboxSelectMultiple()
        }

    age_class = forms.ModelChoiceField(
        empty_label=None,
        queryset=CompetitionClass.objects.filter(category='age'),
        widget=forms.RadioSelect)
    comp_class = forms.ModelChoiceField(
        empty_label=None,
        queryset=CompetitionClass.objects.filter(category='competition'),
        widget=forms.RadioSelect)

    def __init__(self, *args, **kwargs):
        """
        Setup custom data for the form
        """
        super(SeasonalPatrolForm, self).__init__(*args, **kwargs)
        
        # Populate 'age_class' and 'comp_class'
        if 'instance' in kwargs:
            inst = kwargs['instance']
            try:
                self.fields['age_class'].initial = inst.competition_classes.get(category='age')
                self.fields['comp_class'].initial = inst.competition_classes.get(category='competition')
            except Exception:
                # We assumed that only one object per category is stored on the m2m field
                pass

        # Limit choices for 'members'
        # try:
        #     patrol = Patrol.objects.get(owner=self.request.user)
        #     self.fields['members'].queryset = PatrolMember.objects.filter(patrol=patrol)
        # except Patrol.DoesNotExist:
        #     self.fields['members'].queryset = PatrolMember.objects.none()

    def save(self, commit=True):
        """
        Save data from custom fields to model instance
        """
        self.cleaned_data['competition_classes'] = [
            self.cleaned_data['age_class'],
            self.cleaned_data['comp_class'],
        ]
        return super(SeasonalPatrolForm, self).save(commit=commit)
