from datetime import timedelta

from django.core.exceptions import ValidationError
from django.contrib.auth.models import User

from adventurescout_league.apps.accounts.models import LeagueProfile
from adventurescout_league.apps.league.models import Season, CompetitionClass
from adventurescout_league.apps.patrols.serializers import SeasonalPatrolSerializer
from adventurescout_league.apps.patrols.models import Patrol, PatrolMember, SeasonalPatrol
from adventurescout_league.apps.tokens.models import Token

ONE_DAY = timedelta(1, 0)

def create_patrol_token(seasonal_patrol, lifetime=ONE_DAY):
    """
    Create a token containing the data for the seasonal patrol
    and return the token
    """
    s = SeasonalPatrolSerializer(seasonal_patrol)
    token = Token.objects.create('patrol_token', s.data, lifetime)
    token.save()

    return token

# def _get_object_or_error(instance, model):
#     if pk in instance:
#         try:
#             return model.objects.get(pk=instance.pk)
#         except model.DoesNotExist as error:
#             raise AutoSeasonalPatrolError(error)
#     raise AutoSeasonalPatrolError('{} does not exist in db.'.format(instance))

# class AutoSeasonalPatrolError(Exception):
#     pass

# class AutoSeasonalPatrol(object):
#     """
#     Auto creates a seasonal patrol
#     given the correct data

#     Should include:

#     * patrol:               A valid patrol object
#     * members:              A valid list of patrol members
#     * competition_classes:  A valid list of competition classes
#     """

#     def __init__(self, patrol, members, competition_classes):
#         assert type(patrol) is Patrol, '\'patrol\' must be instance of \'Patrol\''
#         for m in members:
#             assert type(m) is PatrolMember, '{} must be instance of \'Patrol\''.format(m)
#         for c in competition_classes:
#             assert type(c) is CompetitionClass, '{} must be instance of \'CompetitionClass\''.format(c)

#         self.patrol = patrol
#         self.competition_classes = competition_classes
#         self.members = members

#     def validate(self):
#         # Test if patrol and members are qualified objects
#         self.patrol.full_clean(exclude='owner')
#         for m in self.members:
#             m.full_clean(exclude='patrol')

#         # Test if competition_classes exists in the db
#         for c in self.competition_classes:
#             if c.pk: 
#                 try:
#                     CompetitionClass.objects.get(pk=c.pk)
#                 except CompetitionClass.DoesNotExist:
#                     raise ValidationError('{} does not exists in the db'.format(c))
#             else:
#                 raise ValidationError('{} does not exists in the db'.format(c))                    
                
#     def save(self):
#         """
#         Try to save the objects one by one

#         """
#         self.validate()

#         # Create user
#         patrol.owner = self.create_user(self.members[0])

#     def create_user(self, attrs):
#         user = User(**attrs.__dict__)
#         user.full_clean()

#         profile = Profile(user=user, **attrs.__dict__)
#         profile.full_clean()

#         user.save()
#         profile.save()

#         return user

