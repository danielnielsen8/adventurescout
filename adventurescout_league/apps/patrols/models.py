# -*- coding: utf-8

from __future__ import unicode_literals

import re

from django.http import Http404
from django.core.urlresolvers import reverse
from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.functional import cached_property
from django.utils.translation import ugettext as _
from django.contrib.auth.models import User

from adventurescout_league.apps.tokens import register
from adventurescout_league.apps.league.models import Season, SeasonalBase
from adventurescout_league.apps.league.check_code import damm_check, damm_extract


# Register tokens
@register('patrol_token')
def patrol_token_handler(request, token):
    # If token has expired raise '404 Not Found'
    if token.has_expired:
        raise Http404
    return token.data


# An scout patrol
# - Basic / static infomation about the patrol
@python_2_unicode_compatible
class Patrol(models.Model):

    class Meta:
        verbose_name = _('Patrol')
        verbose_name_plural = _('Patrols')

    # Fields
    name = models.CharField(max_length=50, unique=True, verbose_name=_('Name'))
    description = models.TextField(blank=True, verbose_name=_('Description'))
    core = models.CharField(max_length=50, blank=True, verbose_name=_('Core'))
    division = models.CharField(max_length=50, blank=True, verbose_name=_('Division'))
    group = models.CharField(max_length=50, blank=True, verbose_name=_('Group'))

    created = models.DateField(auto_now_add=True, verbose_name=_('Created'))
    owner = models.OneToOneField(
        User, unique=True, related_name='patrol', verbose_name=_('Owner'), on_delete=models.CASCADE
    )

    # Descriptions
    name.help_text = u'Indtast jeres patruljenavn. Forsøg at finde på et kort og unikt navn.'
    description.help_text = u'Indtast en kort beskrivelse af jeres patrulje. Hvor i kommer fra og lign. (valgfrit)'
    core.help_text = u'Indtast korps. (valgfrit)'
    division.help_text = 'Indtast division. (valgfrit)'
    group.help_text = 'Indtast gruppe. (valgfrit)'

    def __str__(self):
        return self.name

    @cached_property
    def seasonal_list(self):
        """
        Returns a list of all seasonal races 
        belonging to this Patrol
        """
        if hasattr(self, '_seasonal_list'):
            return self._seasonal_list
        else:
            self._seasonal_list = SeasonalPatrol.objects.filter(patrol=self)
            return self._seasonal_list

    def get_seasonal(self, season):
        """
        Returns the Seasonal patrol with season = *season*
        """
        seasonal = [obj for obj in self.seasonal_list if obj.season == season]
        if len(seasonal) == 1:
            return seasonal[0]
        else:
            return None

    def detail_list(self, fields=None):
        """
        Return a list of tuples showing relevant Information
        about this object - leaving out unimportant fields
        """
        return (
            (_('Name'), self.name,),
            (_('Core'), self.core),
            (_('Division'), self.division,),
            (_('Group'), self.group,),
        )

    def get_absolute_url(self):
        return reverse('patrol_detail', kwargs={'pk': self.pk})


@python_2_unicode_compatible
class PatrolMember(models.Model):
    """
    A member of the Scout Patrol 
    """
    SEX_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female')),
    )

    name = models.CharField(max_length=128)
    sex = models.CharField(max_length=1, choices=SEX_CHOICES)
    phone = models.CharField(max_length=128)
    email = models.EmailField(max_length=254)
    birth_date = models.DateField()
    patrol = models.ForeignKey(Patrol, related_name='members', on_delete=models.CASCADE)
    user = models.OneToOneField(
        User, blank=True, null=True, unique=True, verbose_name=_('user'),
        related_name='patrol_member', on_delete=models.CASCADE
    )

    name.verbose_name = _('Name')
    sex.verbose_name = _('Sex')
    phone.verbose_name = _('Phone')
    email.verbose_name = _('Email')
    birth_date.verbose_name = _('Birth date')

    def __str__(self):
        return '{} ({})'.format(self.name, self.patrol)

    @classmethod
    def create_member(cls, user, patrol):
        """
        Auto create a new member given a 'user' and a 'patrol'
        the member will not be saved
        """
        try:
            profile = user.profile.all()[0]
        except IndexError:
            raise Exception('User does not have a profile')

        member = PatrolMember(
            name=profile.full_name,
            sex=profile.sex,
            phone=profile.phone,
            email=user.email,
            birth_date=profile.birth_date,
            #is_patrol_leader=True,
            patrol=patrol,
            user=user
            )
        return member


class SeasonalPatrolManager(models.Manager):
    def get_by_user(self, user, season=False):
        if not season:
            season = Season.get_current_season()

        queryset = super(SeasonalPatrolManager, self).get_query_set()
        return queryset.get(patrol__owner=user, season=season)

    def get_by_league_id(self, league_id, new_format=True):
        # new format uses damm check digit
        serial_num = None
        season_num = None

        if new_format:
            match = re.match(r'^(?P<season_num>\d{2})(?P<class_age>[A-z]){1}(?P<serial_code>\d{4})$', league_id)
            if match:
                d = match.groupdict()
                if damm_check(d['serial_code']):
                    serial_num = damm_extract(d['serial_code']);
                    season_num = d['season_num']
        else:
            match = re.match(r'^(?P<season_num>\d{2})[A-z]{2}(?P<serial_num>\d{3})$', league_id)
            if match:
                d = match.groupdict()
                serial_num = d['serial_num']
                season_num = d['season_num']

        if serial_num and season_num:
            return self.get(season__number=season_num, 
                            serial_num=serial_num)
        else:
            raise Exception('The provided leagueID is not valid: {}'.format(league_id))


# A seasonal patrol
# - Information about a patrols annual subscription (to the league)
# to a season
# - Inherits from the Seasonal Base in the league app
@python_2_unicode_compatible
class SeasonalPatrol(SeasonalBase):

    class Meta:
        verbose_name = _('Seasonal Patrol')
        verbose_name_plural = _('Seasonal Patrols')
        unique_together = ('patrol', 'season')

    # Fields
    patrol = models.ForeignKey(
        Patrol, related_name='seasonal_patrols', verbose_name=_('Patrol'), on_delete=models.CASCADE
    )
    season = models.ForeignKey(
        Season, related_name='seasonal_patrols',
        verbose_name=_('Season'), on_delete=models.CASCADE
    )
    members = models.ManyToManyField(PatrolMember)

    members.help_text = ''

    objects = models.Manager()
    seasonals = SeasonalPatrolManager()

    def __str__(self):
        return u'{1:s} : {0:s}'.format(self.patrol.name, self.get_league_id())

    def detail_list(self):
        return (
            (_('LeagueID'), self.get_league_id(),),
            (_('Classes'), self.competition_classes),
            ('-', '',),
            (_('Core'), self.patrol.core,),
            (_('Division'), self.patrol.division,),
            (_('Group'), self.patrol.group,),
        )

    def num_members(self):
        return self.members.count()

    def get_absolute_url(self):
        return reverse('patrol_detail', kwargs={'pk': self.pk})

    def get_league_id(self):
        """
        LeagueId in the format XXCYYYY where XX=season, C=class_age, YYYY=serial_num
        """
        kls_age = [kls.identifier for kls in self.get_competition_classes() if kls.category == 'age']
        kls_competition = [kls.identifier for kls in self.get_competition_classes() if kls.category == 'competition']

        class_age = kls_age[0] if kls_age else '?'
        class_competition = kls_competition[0] if kls_competition else '?'
        
        if self.season.use_new_league_id():
            return '{:02d}{}{:04d}'.format(self.season.number, class_age, self.get_code_num())
        else:
            return '{:02d}{}{}{:03d}'.format(self.season.number, class_age, class_competition, self.serial_num)

    @classmethod
    def get_object(cls, **kwargs):
        if re.match(r'^\d{2}[A-z]{2}\d{3}$', kwargs['league_id']): # Old
            return cls.seasonals.get_by_league_id(kwargs['league_id'], False)
        elif re.match(r'^\d{2}[A-z]{1}\d{4}$', kwargs['league_id']): # New
            return cls.seasonals.get_by_league_id(kwargs['league_id'])
        else:
            raise Exception('The provided leagueID is not valid: {}'.format(kwargs['league_id']))
