# -*- coding: utf-8
from django.contrib import admin
from adventurescout_league.apps.patrols.models import Patrol, PatrolMember, SeasonalPatrol


class PatrolAdmin(admin.ModelAdmin):
    list_display = ('name', 'created','owner',)
    search_fields = ('name',)


class PatrolMemberAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'phone', 'patrol',)
    search_fields = ('name', 'email', 'phone', 'patrol__name',)


class SeasonalPatrolAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'season', 'get_league_id', 'patrol',)
    list_filter = ('season',)
    search_fields = ('patrol__name',)


admin.site.register(Patrol, PatrolAdmin)
admin.site.register(PatrolMember, PatrolMemberAdmin)
admin.site.register(SeasonalPatrol, SeasonalPatrolAdmin)
