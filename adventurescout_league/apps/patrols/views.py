# -*- coding: utf-8
from django.conf import settings
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext as _
from django.views.generic.edit import CreateView, UpdateView
from django.forms.models import modelform_factory
from django.forms.formsets import formset_factory

from adventurescout_league.apps.league.views import *
from adventurescout_league.apps.patrols.models import *
from adventurescout_league.apps.patrols.forms import *
from adventurescout_league.apps.patrols.tables import *
from adventurescout_league.apps.utils.widgets import TextPlaceholderInput, DatePicker


def test_view(request):
    from django.http import HttpResponse
    from adventurescout_league.apps.patrols.models import SeasonalPatrol
    from adventurescout_league.apps.patrols.utils import create_patrol_token
    from datetime import timedelta

    token = create_patrol_token(SeasonalPatrol.objects.all()[0], timedelta(100))

    return HttpResponse(token)


class UserPatrolDoesNotExist(Exception):
    msg = 'No patrol has been created for this user'

# ------------------------------------------------------- #
# Base views                                              #
# ------------------------------------------------------- #


class PatrolBaseFormMixin(LoginRequiredMixin, PermissionRequiredMixin):
    """
    Base patrol form mixin
    """
    extra_context = None

    def get_success_url(self):
        return reverse('accounts_dashboard')

    def get_context_data(self, **kwargs):
        context = super(PatrolBaseFormMixin, self).get_context_data(**kwargs)
        context.update(self.extra_context)
        return context

    def get_user_patrol(self):
        try:
            patrol = Patrol.objects.get(owner=self.request.user)
        except Patrol.DoesNotExist:
            raise UserPatrolDoesNotExist
        return patrol

    def get_current_season(self):
        return Season.get_current_season()


class PatrolBaseCreateView(PatrolBaseFormMixin, CreateView):
    pass


class PatrolBaseUpdateView(PatrolBaseFormMixin, UpdateView):
    def form_valid(self, form):
        self.object = self.get_object()
        form.save(self.object)

        return HttpResponseRedirect(self.get_success_url())

# ------------------------------------------------------- #
# List views                                              #
# ------------------------------------------------------- #


class PatrolListView(LeagueListView):
    model = SeasonalPatrol
    table_class = PatrolTable
    thumb_item_template_name = 'patrols/patrol_list_item.html'

    extra_context = {
        'page_headline': 'Patruljeoversigt',
        'page_description': 'Oversigt over patruljernes sæsontilmeldinger til ligaen'
    }

    allowed_filters = {
        'class_age': 'competition_classes__identifier__iexact',
        'class_competition': 'competition_classes__identifier__iexact',
        'season': 'season__number',
        'search': 'patrol__name__icontains'
    }

# ------------------------------------------------------- #
# Patrols                                                 #
# ------------------------------------------------------- #


class PatrolDetailView(LeagueDetailView):
    model = Patrol
    context_object_name = 'patrol'
    template_name = 'patrols/patrol_detail.html'

    def get_object(self, **kwargs):
        # Get the patrol either by
        # direct 'pk' or through
        # the 'league id'
        if 'pk' in self.kwargs:
            return get_object_or_404(self.model, pk=self.kwargs['pk'])
        elif 'league_id' in self.kwargs:
            seasonal_patrol = SeasonalPatrol.get_object(league_id=self.kwargs['league_id'])
            return seasonal_patrol.patrol


class PatrolCreateView(PatrolBaseCreateView):
    model = Patrol
    extra_context = {
        'page_title': _('Create patrol')
    }
    required_perms = ('patrols.add_patrol',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('owner',))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.owner = self.request.user
        self.object.save()

        # Auto create new member from user
        # member = PatrolMember.create_member(self.request.user, self.object)
        # member.save()

        return HttpResponseRedirect(self.get_success_url())


class PatrolUpdateView(PatrolBaseUpdateView):
    model = Patrol
    extra_context = {
        'page_title': _('Update patrol')
    }
    required_perms = ('patrols.change_patrol',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('owner',))

# ------------------------------------------------------- #
# Patrol members                                          #
# ------------------------------------------------------- #


class PatrolMemberCreateView(PatrolBaseCreateView):
    model = PatrolMember
    extra_context = {
        'page_title': _('Create patrol member'),
    }
    required_perms = ('patrols.add_patrolmember',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('is_patrol_leader', 'patrol', 'user',))

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.patrol = self.get_user_patrol()
        self.object.save()

        return HttpResponseRedirect(self.get_success_url())


class PatrolMemberUpdateView(PatrolBaseUpdateView):
    model = PatrolMember
    extra_context = {
        'page_title': _('Update patrol member'),
    }
    required_perms = ('patrols.change_patrolmember',)

    def get_form_class(self):
        return modelform_factory(self.model, exclude=('is_patrol_leader', 'patrol', 'user',))


# ------------------------------------------------------- #
# Seasonal Patrols                                        #
# ------------------------------------------------------- #


class SeasonalPatrolCreate(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'patrols/seasonalpatrol_form.html'
    extra_context = {
        'page_title': _('Create Seasonal Patrol')
    }
    required_perms = ('patrols.change_seasonalpatrol',)

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.season = Season.objects.get(number=kwargs['season_num'])
        return super(SeasonalPatrolCreate, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        context = self.get_context()
        context['form'] = SeasonalPatrolForm(request=self.request)
        context['member_formset'] = self.get_member_formset()(initial=[
            self.get_initial_member(request.user)
        ])
        return self.render_to_response(context=context)

    def post(self, request, **kwargs):
        form = SeasonalPatrolForm(request=self.request, data=self.request.POST)
        MemberFormset = self.get_member_formset()
        formset = MemberFormset(request.POST)

        if form.is_valid() and formset.is_valid():
            self.object = form.save(commit=False)
            self.object.season = self.season
            self.object.patrol = Patrol.objects.get(owner=self.request.user)
            self.object.save()
            form.save_m2m()

            for row in formset.cleaned_data:
                if row:
                    member = PatrolMember(**row)
                    member.patrol = Patrol.objects.get(owner=request.user)
                    member.save()
                    self.object.members.add(member)
            return HttpResponseRedirect(reverse('accounts_dashboard'))

        context = self.get_context()
        context['form'] = form
        context['member_formset'] = formset
        return self.render_to_response(context=context)

    def get_context(self):
        context = {}
        context.update(self.extra_context)
        return context

    def get_member_formset(self):
        set_extra = settings.SEASONAL_PATROL_MAX_MEMBERS
        set_max = settings.SEASONAL_PATROL_MAX_MEMBERS
        member_form = modelform_factory(PatrolMember, exclude=('is_patrol_leader', 'patrol', 'user',),
                                                      widgets={'name': TextPlaceholderInput(text=_('Name')),
                                                               'phone': TextPlaceholderInput(text=_('Phone')),
                                                               'email': TextPlaceholderInput(text=_('E-mail')),
                                                               'birth_date': DatePicker})
        member_formset = formset_factory(member_form, extra=set_extra, max_num=set_max)
        return member_formset

    def get_initial_member(self, user):
        try:
            profile = user.profile.all()[0]
            return {
                'name': profile.full_name,
                'sex': profile.sex,
                'phone': profile.phone,
                'email': user.email,
                'birth_date': '{:%d.%m.%Y}'.format(profile.birth_date),
            }
        except IndexError:
            return {}


class SeasonalPatrolUpdate(LoginRequiredMixin, PermissionRequiredMixin, TemplateView):
    template_name = 'patrols/seasonalpatrol_form.html'
    extra_context = {
        'page_title': _('Update Seasonal Patrol')
    }
    required_perms = ('patrols.change_seasonalpatrol',)

    def dispatch(self, request, *args, **kwargs):
        self.request = request
        self.pk = kwargs['pk']
        self.object = self.get_object()
        return super(SeasonalPatrolUpdate, self).dispatch(request, *args, **kwargs)

    def get(self, request, **kwargs):
        context = self.get_context()
        context['member_formset'] = self.get_member_formset()
        return self.render_to_response(context=context)

    def post(self, request, **kwargs):
        MemberFormset = self.get_member_formset()
        formset = MemberFormset(request.POST)
        if formset.is_valid():
            for row in formset.cleaned_data:
                if row:
                    member = PatrolMember(**row)
                    member.patrol = Patrol.objects.get(owner=request.user)
                    member.save()
                    self.object.members.add(member)
            return HttpResponseRedirect(reverse('accounts_dashboard'))

        context = self.get_context()
        context['form'] = SeasonalPatrolForm(request=request)
        context['member_formset'] = formset
        return self.render_to_response(context=context)

    def get_context(self):
        context = {}
        context.update(self.extra_context)

        competition_classes = {}
        for comp_cls in self.object.get_competition_classes_sorted():
            competition_classes[comp_cls['category'][0]] = ' '.join([str(c) for c in comp_cls['list']])

        context['competition_classes'] = competition_classes
        members = self.object.members.all()
        context['members'] = members

        return context

    def get_object(self):
        try:
            sp = SeasonalPatrol.objects.get(pk=self.pk)
            if sp.patrol.owner.pk != self.request.user.pk:
                raise PermissionDenied
            return sp
        except SeasonalPatrol.DoesNotExist:
            raise Http404

    def get_member_formset(self):
        set_extra = settings.SEASONAL_PATROL_MAX_MEMBERS - self.object.members.count()
        member_form = modelform_factory(PatrolMember, exclude=('is_patrol_leader', 'patrol', 'user',),
                                                      widgets={'name': TextPlaceholderInput(text=_('Name')),
                                                               'phone': TextPlaceholderInput(text=_('Phone')),
                                                               'email': TextPlaceholderInput(text=_('E-mail')),
                                                               'birth_date': DatePicker})
        member_formset = formset_factory(member_form, extra=set_extra)
        return member_formset
