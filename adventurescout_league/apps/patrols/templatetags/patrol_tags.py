# -*- coding: utf-8
from django import template
from django.utils.translation import ugettext as _
from adventurescout_league.apps.patrols.models import *
from adventurescout_league.apps.patrols.tables import *
from adventurescout_league.apps.league.models import Season

register = template.Library()


@register.inclusion_tag('patrols/includes/patrol_current_seasonal.html')
def patrol_current_seasonal(patrol):
    season = Season.get_current_season()

    try:
        seasonal_patrol = SeasonalPatrol.objects.get(patrol=patrol, season=season)
    except SeasonalPatrol.DoesNotExist:
        seasonal_patrol = None
    return {'seasonal_patrol': seasonal_patrol, 'season': season}


@register.inclusion_tag('patrols/includes/patrol_members.html')
def patrol_members(patrol, season=False):
    """
    Show (public) patrol members table
    If season is given, only shows members for that season
    """
    context = {}
    tb = False
    if season:
        try:
            sp = SeasonalPatrol.objects.get(patrol=patrol, season=season)
            if sp.members.count() > 0:
                tb = PatrolMembersTable(sp.members.all())
        except SeasonalPatrol.DoesNotExist:
            pass
    else:
        tb = PatrolMembersTable(patrol.members.all())

    context['season'] = season
    context['member_table'] = tb
    return context


@register.inclusion_tag('patrols/includes/user_patrol.html')
def user_patrol(user):
    """
    Show (private) patrol for user
    """
    try:
        patrol = Patrol.objects.get(owner=user)
    except Patrol.DoesNotExist:
        patrol = False
    
    return {'patrol': patrol}


@register.inclusion_tag('patrols/includes/user_patrol_members.html')
def user_patrol_members(user, request):
    """
    Show patrol members associated with users patrol
    or help text
    """    
    context = {'request': request}
    try:
        patrol = Patrol.objects.get(owner=user)
        context['member_table'] = UserPatrolMembersTable(patrol.members.all())
    except Patrol.DoesNotExist:
        context['action_needed'] = 'Det er nødvendigt at oprette en patrulje, før du kan oprette patruljemedlemmer!'
    return context


@register.inclusion_tag('patrols/includes/user_seasonal_patrols.html')
def user_seasonal_patrols(user, request):
    """
    Show table of seasonal patrols associated with user
    or help text
    """
    context = {'request': request}
    
    # Make sure that the user only have one patrol associated!
    try:
        patrol = Patrol.objects.get(owner=user)
    except Patrol.DoesNotExist:
        patrol = None
        context['action_needed'] = 'Det er nødvendigt at oprette en patrulje, før du kan oprette sæsonpatruljer!'
    except Patrol.MultipleObjectsReturned:
        patrol = None
        context['action_needed'] = 'Det er sket en fejl. Du har mere end en patrulje tilknyttet. Kontakt venligst support@adventurespejd.dk'

    if patrol:
        seasons = Season.objects.all()
        data = []

        # Iterate over seasons
        for season in seasons:
            actions = False

            try:
                seasonal_patrol = SeasonalPatrol.objects.get(season=season, patrol=patrol)
            except SeasonalPatrol.DoesNotExist:
                seasonal_patrol = None
            except SeasonalPatrol.MultipleObjectsReturned:
                seasonal_patrol = None
                context['action_needed'] = 'Det er sket en fejl! Du har mere end en sæson patrulje for season: {}' \
                                            .format(season)

            # Determine status
            if seasonal_patrol:
                if season.is_active():
                    status = _('Active')
                else:
                    status = _('Inactive')
            else:
                status = _('Not created')

            # Determine actions
            if seasonal_patrol:
                if season.registration_is_open():
                    actions = {
                        'op': 'edit',
                        'pk': seasonal_patrol.pk
                    }
            else:
                if season.registration_is_open():
                    actions = {
                        'op': 'create',
                        'season_num': season.number
                    }                    

            data.append({
                'season': '{} : {}'.format(season.number, season.name),
                'seasonal_patrol': seasonal_patrol,
                'status': status,
                'actions': actions
            })
        context['seasonalpatrol_table'] = UserSeasonalPatrolsTable(data)
    return context