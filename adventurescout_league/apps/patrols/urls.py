# -*- coding: utf-8
from django.conf.urls import url

from adventurescout_league.apps.patrols.views import (
    PatrolCreateView, PatrolDetailView, PatrolListView, PatrolMemberCreateView,
    PatrolMemberUpdateView, PatrolUpdateView, SeasonalPatrolCreate, SeasonalPatrolUpdate,
    test_view,
)

urlpatterns = [
    
    # Test
    url(r'^test/$', test_view),

    # Lists
    url(r'^list/$', PatrolListView.as_view(), name="patrol_list"),
    # Detail
    url(r'^detail/(?P<pk>\d+)/$', PatrolDetailView.as_view(), name="patrol_detail"),
    # Get by leagueID
    url(r'^(?P<league_id>\d{2}\w{2}\d{3})/$', PatrolDetailView.as_view(), name="patrol_detail_by_league_id"),
    
    url(r'^create/$', PatrolCreateView.as_view(), name="patrol_create"),
    url(r'^edit/(?P<pk>\d+)/$', PatrolUpdateView.as_view(), name="patrol_edit"),

    # Patrol members
    url(r'^members/create/$', PatrolMemberCreateView.as_view(), name='patrol_member_create'),
    url(r'^members/edit/(?P<pk>\d+)/$', PatrolMemberUpdateView.as_view(), name='patrol_member_edit'),

    # Seasonal Patrols
    url(r'^seasonal/(?P<season_num>\d+)/create/$', SeasonalPatrolCreate.as_view(), name='seasonal_patrol_create'),
    url(r'^seasonal/edit/(?P<pk>\d+)/$', SeasonalPatrolUpdate.as_view(), name='seasonal_patrol_edit')
]
