# -*- coding: utf-8 -*-
# Generated by Django 1.11.23 on 2020-01-11 16:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patrols', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='patrol',
            name='name',
            field=models.CharField(help_text='Indtast jeres patruljenavn. Fors\xf8g at finde p\xe5 et kort og unikt navn.', max_length=50, unique=True, verbose_name='Navn'),
        ),
    ]
