# -*- coding: utf-8

from django.forms import widgets
from rest_framework import serializers
from adventurescout_league.apps.league.serializers import *
from adventurescout_league.apps.league.models import Season, CompetitionClass
from adventurescout_league.apps.patrols.models import Patrol, PatrolMember, SeasonalPatrol


class PatrolSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patrol
        exclude = ('id', 'owner',)


class PatrolMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatrolMember
        exclude = ('id', 'patrol', 'user',)


class PublicPatrolMemberSerializer(serializers.ModelSerializer):
    class Meta:
        model = PatrolMember
        exclude = ('id', 'patrol', 'user', 'phone', 'email',)

    birth_date = serializers.DateField(format='%Y')


class PublicSeasonalPatrolSerializer(serializers.Serializer):
    league_id = serializers.CharField(source='get_league_id', read_only=True, required=False)
    created = serializers.DateField(required=False)

    season = SeasonSerializer(required=False)
    patrol = PatrolSerializer()
    competition_classes = CompetitionClassIdentifierSerializer(many=True)
    members = PublicPatrolMemberSerializer(many=True)

    def __init__(self, *args, **kwargs):
        include_month = kwargs.pop('include_month', False)
        super(PublicSeasonalPatrolSerializer, self).__init__(*args, **kwargs)

        if include_month:
            self.fields['members'].fields['birth_date'].format = '%b %Y'


class SeasonalPatrolSerializer(serializers.Serializer):
    league_id = serializers.CharField(source='get_league_id', read_only=True, required=False)
    created = serializers.DateField(required=False)

    season = SeasonSerializer(required=False)
    patrol = PatrolSerializer()
    competition_classes = CompetitionClassIdentifierSerializer(many=True)
    members = PatrolMemberSerializer(many=True)