"user strict";

var Widget = new Class({
    options: {
        dataUrl: '',
        synchronize: true
    },
    fetchData: function() {
        var req = new Request.JSON({
            url: this.options.dataUrl,
            method: 'GET'
        });
        req.addEvent('success', this.handleData.bind(this));
        req.send();
    },
    initWidget: function() {
        this.dom = {
            header: this.elem.getElement('header'),
            season: this.elem.getElement('header .season'),
            select: new Element('select', {
                class: 'season-select'
            })
        };
        // Fetch data
        var req = new Request.JSON({
            url: this.options.dataUrl,
            method: 'GET'
        });
        req.addEvent('success', function(response) {
            this.data = response;

            if (this.data.length === 0) {
                this.showEmpty();
            } else {
                this.initSelect();    
            }
        }.bind(this));
        req.send();
    },
    initSelect: function() {
        this.dom.header.grab(new Element('div.select-field')
            .grab(this.dom.select));

        this.data.each(function(elem, i) {
            var option = new Element('option', {
                value: i,
                html: elem.season.unicode_str
            });
            this.dom.select.grab(option);
        }, this);
        this.dom.select.addEvent('change', this.selectChange.bind(this));

        if (this.options.synchronize) {
            document.addEvent('seasonchanged', this.seasonChange.bind(this));
        }
        this.update(this.data[0]);   
    },
    selectChange: function(event) {
        var i = event.target.value;

        if (this.options.synchronize) {
            document.fireEvent('seasonchanged', i);
        } else {
            var elem = this.data[i];
            this.update(elem);
        }
    },
    seasonChange: function(i) {
        var elem = this.data[i];
        // Avoid changing to season that is not defined!
        // A race may have a seasonal race for season 1, 
        // but not an admission for that season!
        if (elem) {
            this.dom.select.set('value', i);
            this.update(elem);
        }
    },
    update: function(elem) {
        // Do nothing - override this func
    },

    showEmpty: function() {
        // Do nothing - override this func
    }
});


var SeasonalAdmissionWidget = new Class({
    Implements: [Widget, Options, Events],

    templates: {
        body:       '<div class="alert">{status}</div>' + 
                    '<div class="competition-classes"><p>Du kan tilmelde dig som:</p><ul>{comp_classes}</ul></div>',
        compCls:    '<li>{comp_cls}</li>',
        button:     '<a class="button red" href="{link}">Gå til tilmelding</a>'
    },

    initialize: function(elem, options) {
        this.setOptions(options);
        this.elem = elem;        

        this.initWidget();
    },
    
    update: function(elem) {
        var dom = this.dom;
        dom.season.set('html', elem.season.unicode_str);
        
        // Comp classes
        var cc = '';
        elem.competition_classes.each(function(el, i) {
            cc += this.templates.compCls.substitute({
                comp_cls: [el.identifier, el.name].join(' : ')
            });
        }, this);

        var data = {
            status: elem.status,
            comp_classes: cc,
        };
        if (elem.is_open) {
            data.link = elem.link;
        }
        this.render(data);
    },

    render: function(data) {
        this.elem.getChildren(':not(header)').dispose();
        var elems = Elements.from(this.templates.body.substitute(data));
        if (data.link) {
            elems.append(Elements.from(this.templates.button.substitute(data)));
        }
        elems.inject(this.elem, 'bottom');
    },

    showEmpty: function() {
        new Element('div.alert', {
            'html': 'Der findes ingen sæsonoptagelser for dette løb!'
        }).inject(this.elem, 'bottom');
    }
});


var SeasonalPatrolMembersWidget = new Class({
    Implements: [Widget, Options, Events],

    templates: {
        table:      '<p>Patruljemedlemmer tilmeldt for denne sæson.</p>' +
                    '<table><thead><tr>' +
                    '<th class="name">Navn</th>' +
                    '<th class="sex">Køn</th>' +
                    '<th class="birth-date">Fødselsdag</th>' +
                    '</tr></thead><tbody>{rows}</tbody></table>',
        row:        '<tr><td class="name">{name}</td><td class="sex">{sex}</td><td class="birth-date">{birth_date}</td></tr>'
    },

    initialize: function(elem, options) {
        this.setOptions(options);
        this.elem = elem;        

        this.initWidget();        
    },
    
    update: function(elem) {
        var dom = this.dom;
        dom.season.set('html', elem.season.unicode_str);
        
        // Rows
        var rows = '';
        elem.members.each(function(item, i) {
            switch (item.sex) {
                case 'M':
                    item.sex = 'Dreng';
                    break;
                case 'F':
                    item.sex = 'Pige';
                    break;
            }
            rows += this.templates.row.substitute(item);
        }, this);
        var data = {
            rows: rows
        };
        this.render(data);
    },

    render: function(data) {
        this.elem.getChildren(':not(header)').dispose();
        var elems = Elements.from(this.templates.table.substitute(data));
        elems.inject(this.elem, 'bottom');
    }
});


/*
 * Base for interactive widgets of 
 * seasonal objects belonging to a given object
 */
var SeasonalBaseWidget = new Class({
    Implements: [Widget, Options, Events],

    templates: {
        body:       '<div class="meta">{fields}</div>',
        metaField:  '<div class="field"><span class="label grey">{label}</span><span class="value">{value}</span></div>'
    },

    initialize: function(elem, options) {
        this.setOptions(options);
        this.elem = elem;
        this.initWidget();
    },

    // Update widget
    update: function(elem) {
        var dom = this.dom;
        dom.season.set('html', elem.season.unicode_str);
        this.render(this.getMetaFields(elem));
    },

    getMetaFields: function(elem) {
        return {};
    },

    // Render template
    render: function(data) {
        var fields = '';
        Object.each(data, function(value, key) {
            fields += this.templates.metaField.substitute({
                'label': key,
                'value': value                
            });
        }, this);
        this.elem.getElements('.meta').dispose();
        Elements.from(this.templates.body.substitute({fields: fields}))
            .inject(this.elem, 'bottom');
    }
});


/*
 * Interactive widget of 
 * seasonal objects belonging to a patrol
 */
var SeasonalPatrolWidget = new Class({
    Extends: SeasonalBaseWidget,
    initialize: function(elem, options) {
        this.parent(elem, options);
    },
    getMetaFields: function(elem) {
        var comp_classes = [];
        elem.competition_classes.each(function(elem, i) {
            comp_classes.push(elem.identifier + ' : ' + elem.name);
        });        
        var data = {
            'LigaID': elem.league_id,
            'Klasser': comp_classes.join(', '),
            'Antal medlemmer': elem.members.length + ' <span class="grey">(for denne sæson)</span>',
            '-': '&nbsp;',
            'Navn': elem.patrol.name,
            'Korps': elem.patrol.core,
            'Division': elem.patrol.division,
            'Gruppe': elem.patrol.group
        };
        return data;
    }
});


/*
 * Interactive widget of 
 * seasonal objects belonging to a race
 */
var SeasonalRaceWidget = new Class({
    Extends: SeasonalBaseWidget,
    initialize: function(elem, options) {
        this.parent(elem, options);
    },
    getMetaFields: function(elem) {
        var comp_classes = [];
        elem.competition_classes.each(function(elem, i) {
            comp_classes.push(elem.identifier + ' : ' + elem.name);
        });
        var start_date = new Date(elem.event_start);
        var end_date = new Date(elem.event_end);
        
        var data = {
            'LigaID': elem.league_id,
            'Klasser': comp_classes.join(', '),
            'Arrangement start': start_date.format('%e. %b %Y - kl. %k:%M'),
            'Arrangement slut': end_date.format('%e. %b %Y - kl. %k:%M'),
            '-': '&nbsp;',
            'Navn': elem.race.name,
        };
        return data;
    }
});
