
// Doc needed
var TabbableContent = new Class({
    initialize: function(element) {
        this.element = element;
        this.navElements = element.getElements('.nav li a');
        this.contentElements = element.getElements('.tab-content .tab-pane');
        
        this.currentNavElement = this.navElements[0];
        this.currentContentElement = this.contentElements[0];

        // Setup click events
        this.navElements.each(function(item, i) {
            item.addEvent('click', this.click.bind(this));
        }, this);
    },

    click: function(event) {
        var elem = event.target,
            id = elem.get('href'),
            contentElem = this.contentElements.filter(id);

        event.preventDefault();

        elem.getParent().addClass('active');
        this.currentNavElement.getParent().removeClass('active');
        this.currentNavElement = elem;

        contentElem.addClass('active');
        this.currentContentElement.removeClass('active');
        this.currentContentElement = contentElem;                
    }
    
});
