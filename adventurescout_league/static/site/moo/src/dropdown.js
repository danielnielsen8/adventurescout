var Dropdown = new Class({
    initialize: function(elem) {
        this.elem = elem;
        this.listFx = new Fx.Slide(this.elem.getElement('ul'), {
            duration: 100,
            transition: Fx.Transitions.Cubic.easeOut
        }).hide();

        // Init dom
        this.elem.addEvent('mouseenter', this.mouseenter.bind(this));
        this.elem.addEvent('mouseleave', this.mouseleave.bind(this));
    },
    mouseenter: function(event) {
        this.listFx.cancel().slideIn();
    },
    mouseleave: function(event) {
        this.listFx.cancel().slideOut();
    }
});
