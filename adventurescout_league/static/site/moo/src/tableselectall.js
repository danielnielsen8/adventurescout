var TableSelectAll = new Class({
    initialize: function(elem) {
        this.table = elem
        this.thCheckbox = this.table.getElement('thead th input[type="checkbox"]')
        this.tdCheckboxes = this.table.getElements('tbody td.include input[type="checkbox"]')

        this.thCheckbox.addEvent('change', function(event) {
            var elem = event.target;
            if (elem.get('checked')) {
                this.selectAll();
            } else {
                this.unselectAll();
            }
        }.bind(this));

        this.tdCheckboxes.addEvent('change', function(event) {
            var elem = event.target;
            if (!elem.get('checked')) {
                this.thCheckbox.set('checked', false)
            }
        }.bind(this));
    },
    selectAll: function() {
        this.tdCheckboxes.each(function(elem) {
            elem.set('checked', true);
        });
    },
    unselectAll: function() {
        this.tdCheckboxes.each(function(elem) {
            elem.set('checked', false);
        });
    }
});
