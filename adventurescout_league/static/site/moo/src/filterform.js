
// Doc needed
var FilterForm = new Class({
    initialize: function(form) {
        this.form = form;
        if (this.form == null) return;

        this.submit = this.form.getElement('input[type="submit"]');
        this.initDom();
    },
    
    initDom: function() {
        // Hide submit
        this.submit.hide();

        // Iterate selects, radios
        this.form.getElements('select, input[type="radio"]').each(function(elem) {
            elem.addEvent('change', this.change.bind(this));
        }, this);
    },

    change: function(event) {
        this.form.submit();
    }
});
