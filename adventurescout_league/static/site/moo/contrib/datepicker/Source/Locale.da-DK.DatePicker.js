/*
---
name: Locale.en-US.DatePicker
description: English Language File for DatePicker
authors: Arian Stolwijk
requires: [More/Locale]
provides: Locale.en-US.DatePicker
...
*/


Locale.define('da-DK', 'DatePicker', {
	select_a_time: 'Vælg en tid',
	use_mouse_wheel: 'Brug hjulet på din mus til hurtigt at skifte værdi',
	time_confirm_button: 'OK',
	apply_range: 'Anvend',
	cancel: 'Anuller',
	week: 'Uge'
});
