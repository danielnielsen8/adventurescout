"use strict";

Locale.use('da-DK');

Behavior.addGlobalFilters({
    Dropdown: {
        setup: function(element, api) {
            return new Dropdown(element);      
        }
    },
    FilterForm: {
        setup: function(element, api) {
            return new FilterForm(element);
        }
    },
    TableSelectAll: {
        setup: function(element, api) {
            return new TableSelectAll(element);
        }
    },
    TabbableContent: {
        setup: function(element, api) {
            return new TabbableContent(element);
        }
    },
    SeasonalAdmissionsWidget: {
        setup: function(element, api) {
            var options = api.getAs(Object, 'options');
            return new SeasonalAdmissionWidget(element, options);
        }
    },
    SeasonalRaceWidget: {
        setup: function(element, api) {
            var options = api.getAs(Object, 'options');
            return new SeasonalRaceWidget(element, options);
        }
    },
    SeasonalPatrolMembersWidget: {
        setup: function(element, api) {
            var options = api.getAs(Object, 'options');
            return new SeasonalPatrolMembersWidget(element, options);
        }
    },    
    SeasonalPatrolWidget: {
        setup: function(element, api) {
            var options = api.getAs(Object, 'options');
            return new SeasonalPatrolWidget(element, options);
        }
    },
    DatePicker: {
        setup: function(element, api) {
            api.setDefault({
                timePicker: false,
                format: '%d.%m.%Y %k:%M',
                startView: 'days'
            });
            var options = {
                timePicker: api.getAs(Boolean, 'timePicker'),
                format: api.get('format'),
                startView: api.get('startView')
            };
            new Picker.Date(element, Object.merge({
                positionOffset: {x: 5, y: -5}, 
                pickerPosition: 'right',
                pickerClass: 'datepicker_dashboard', 
                useFadeInOut: !Browser.ie 
            }, options));
        }
    }
});

// Dropdowns
window.addEvent('domready',function() {
    var behavior = new Behavior()
    behavior.apply(document.body);
});