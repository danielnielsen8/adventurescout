# coding: utf-8

from __future__ import absolute_import

from django.conf.urls import include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings
from django.conf.urls.static import static
from .apps.league.views import FrontView

# Admin
from django.contrib import admin
admin.autodiscover()

urlpatterns = [

    # Frontpage
    url(r'^$', FrontView.as_view(), name='adventurescout-front'),

    # Accounts
    url(r'^accounts/', include('adventurescout_league.apps.accounts.urls')),

    # Patrols
    url(r'^patrol/', include('adventurescout_league.apps.patrols.urls')),

    # Races
    url(r'^race/', include('adventurescout_league.apps.races.urls')),

    # Points
    url(r'^point/', include('adventurescout_league.apps.points.urls')),

    # Registrations
    url(r'^registration/', include('adventurescout_league.apps.registrations.urls')),    

    # League API
    url(r'^api/', include('adventurescout_league.apps.leagueapi.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
]

# Static and Media files in dev
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

# Content pages - catch all unmatched!
urlpatterns += [
    url(r'^', include('adventurescout_league.apps.content.urls')),
]
