"""Base settings shared by all environments"""
# Import global settings to make it easier to extend settings.
from django.conf.global_settings import *   # pylint: disable=W0614,W0401

import os
import sys
import adventurescout_league as project_module

# ==============================================================================
# Generic Django project settings
# ==============================================================================

ADMINS = [
    ('Niels Kristian Jensen', 'nkj@internetgruppen.dk'),
    ('Daniel Nielsen', 'dni@qubix.dk'),
]
MANAGERS = ADMINS

DEBUG = True

SITE_ID = 1
# Local time zone for this installation. Choices can be found here:
# https://en.wikipedia.org/wiki/List_of_tz_zones_by_name
TIME_ZONE = 'Europe/Copenhagen'
USE_TZ = True
USE_I18N = True
USE_L10N = True
LANGUAGE_CODE = 'da-DK'
LANGUAGES = (
    ('da-DK', 'Danish'),
)

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'm(#ui^3)_y!g_t5^m5&amp;3ga_z#1&amp;%0xm1=5(c)mhhq&amp;i704k4we'

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    # Admin
    'django.contrib.admin',
    'django.contrib.admindocs',

    # Contrib
    'django_markup',
    'django_tables2',
    'easy_thumbnails',
    'rest_framework',
    'rest_framework.authtoken',

    # Apps
    'adventurescout_league.apps.accounts',
    'adventurescout_league.apps.league',
    'adventurescout_league.apps.tokens',
    'adventurescout_league.apps.leagueapi',
    'adventurescout_league.apps.patrols',
    'adventurescout_league.apps.races',
    'adventurescout_league.apps.points',
    'adventurescout_league.apps.registrations',
    'adventurescout_league.apps.utils',
    'adventurescout_league.apps.content',
)

# ==============================================================================
# Calculation of directories relative to the project module location
# ==============================================================================

PROJECT_DIR = os.path.dirname(os.path.realpath(project_module.__file__))

PYTHON_BIN = os.path.dirname(sys.executable)
ve_path = os.path.dirname(os.path.dirname(os.path.dirname(PROJECT_DIR)))
# Assume that the presence of 'activate_this.py' in the python bin/
# directory means that we're running in a virtual environment.
if os.path.exists(os.path.join(PYTHON_BIN, 'activate_this.py')):
    # We're running with a virtualenv python executable.
    VAR_ROOT = os.path.join(os.path.dirname(PYTHON_BIN), 'var')
elif ve_path and os.path.exists(os.path.join(ve_path, 'bin',
        'activate_this.py')):
    # We're running in [virtualenv_root]/src/[project_name].
    VAR_ROOT = os.path.join(ve_path, 'var')
else:
    # Set the variable root to a path in the project which is
    # ignored by the repository.
    VAR_ROOT = os.path.join(PROJECT_DIR, 'var')

if not os.path.exists(VAR_ROOT):
    os.mkdir(VAR_ROOT)

# ==============================================================================
# Project URLS and media settings
# ==============================================================================

ROOT_URLCONF = 'adventurescout_league.urls'

LOGIN_URL = '/accounts/login/'
LOGOUT_URL = '/logout/'
LOGIN_REDIRECT_URL = '/accounts/dashboard/'

STATIC_URL = '/static/'
MEDIA_URL = '/media/'

STATIC_ROOT = os.path.join(VAR_ROOT, 'static')
MEDIA_ROOT = os.path.join(VAR_ROOT, 'media')

STATICFILES_DIRS = (
    os.path.join(PROJECT_DIR, 'static'),
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
)

# ==============================================================================
# Templates
# ==============================================================================

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_DIR, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                "django.template.context_processors.request",
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'adventurescout_league.apps.league.context_processors.extra_context',
            ],
        },
    },
]

# ==============================================================================
# Middleware
# ==============================================================================

MIDDLEWARE = [
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

# ==============================================================================
# Auth / security
# ==============================================================================

AUTHENTICATION_BACKENDS += (
    'adventurescout_league.apps.utils.email_backends.EmailAuthBackend',
)

# ==============================================================================
# Miscellaneous project settings
# ==============================================================================

ACCOUNT_ACTIVATION_DAYS = 1
SEASONAL_PATROL_MAX_MEMBERS = 8
MAKE_OWNER_FIRST_SEASONAL_MEMBER = True
DEFAULT_IMAGES = {
    'standard': os.path.join(PROJECT_DIR, 'static/site/img/default-image.png'),
    'wide': os.path.join(PROJECT_DIR, 'static/site/img/default-image-wide.png'),
}
PYTHON_DATETIME_FORMAT = '%d. %b %Y - kl. %H:%M'

AUTH_USER_MODEL = 'auth.User'

# ==============================================================================
# Third party app settings
# ==============================================================================

REST_FRAMEWORK = {
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
    )
}
