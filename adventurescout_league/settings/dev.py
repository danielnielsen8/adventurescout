"""Settings for Development Server"""
from adventurescout_league.settings.base import *   # pylint: disable=W0614,W0401

DEBUG = True

VAR_ROOT = os.path.dirname(os.path.abspath(__file__) + "/../")
MEDIA_ROOT = os.path.join(VAR_ROOT, 'uploads')
STATIC_ROOT = os.path.join(VAR_ROOT, 'static')

# Site url
SITE_URL = 'http://127.0.0.1:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'localhost',
        'PASSWORD': 'docker',
    }
}

WSGI_APPLICATION = 'adventurescout_league.wsgi.dev.application'
