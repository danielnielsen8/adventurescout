"""Settings for Development Server"""
from adventurescout_league.settings.base import *   # pylint: disable=W0614,W0401

# Site url
SITE_URL = 'http://adventurespejd.azurewebsites.net'

DEBUG = False

# VAR_ROOT = '/var/www/adventurescout_league'
MEDIA_ROOT = '/opt/app/wwwroot/media'
STATIC_ROOT = '/opt/app/wwwroot/static'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'adventurespejd',
        'USER': 'adm_adventspejd@sqlpg',
        'PASSWORD': 'internetGruppen20',
        'HOST': 'sqlpg.postgres.database.azure.com',
        'PORT': '5432',
        'OPTIONS': {'sslmode': 'require'},
    }
}

ALLOWED_HOSTS = (
    'adventurespejd.azurewebsites.net',
    'liga.adventurespejd.dk',
    'dev-liga.adventurespejd.dk',
    'test-liga.adventurespejd.dk',
)

# Email settings
EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'adventurescout'
EMAIL_HOST_PASSWORD = '****'
DEFAULT_FROM_EMAIL = 'noreply@adventurespejd.dk'
SERVER_EMAIL = 'admin@adventurespejd.dk'

# Static/media root on webfaction server
# STATIC_ROOT = '/home/macfly/webapps/adventurespejd_dev_static/'
# MEDIA_ROOT = '/home/macfly/webapps/adventurespejd_dev_media/'

# A sample logging configuration. The only tangible logging
# performed by this configuration is to send an email to
# the site admins on every HTTP 500 error when DEBUG=False.
# See http://docs.djangoproject.com/en/dev/topics/logging for
# more details on how to customize your logging configuration.
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'filters': {
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse'
        }
    },
    'handlers': {
        'mail_admins': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],
            'class': 'django.utils.log.AdminEmailHandler'
        },
        'logfile': {
            'class': 'logging.handlers.WatchedFileHandler',
            'filename': '/opt/app/logs/django_errors.log'
        }
    },
    'loggers': {
        'django.request': {
            'handlers': ['mail_admins'],
            'level': 'ERROR',
            'propagate': True,
        },
        'django': {
            'handlers': ['logfile'],
            'level': 'ERROR',
            'propagate': False,
        }
    }
}

# WSGI
WSGI_APPLICATION = 'adventurescout_league.wsgi.production.application'
