"""Settings for Development Server"""
from adventurescout_league.settings.base import *   # pylint: disable=W0614,W0401

DEBUG = True

VAR_ROOT = '/var/www/adventurescout_league'
MEDIA_ROOT = os.path.join(VAR_ROOT, 'uploads')
STATIC_ROOT = os.path.join(VAR_ROOT, 'static')

# Site url
SITE_URL = 'http://127.0.0.1:8000'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': '172.18.0.2',
        'PASSWORD': 'docker',
    }
}

WSGI_APPLICATION = 'adventurescout_league.wsgi.dev.application'
